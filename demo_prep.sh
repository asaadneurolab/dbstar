#!/bin/sh

# last update (v1.0.0a; 2018-02-08): minor documentation changes

# shell script for preparing patient imaging and trajectory data
# (for use with sample data)

# preop CT:
mv 19890323_165201s002a001.nii ct_pre.nii
# preop T1:
mv 19890323_103533SAGT1MPRAGEPOSTGADOs004a1001.nii t1_pre.nii
# intraop CT:
mv 19890323_113106s001a000.nii ct_intra.nii 
# postop MR:
mv 19890323_120306s002a1001.nii t1_post.nii

# note that the intra-op CT was taken with A,C,P tracks
# ...but a M track was ultimately placed (hence the *ct_traj files)

# recording site trajectory depths
echo "5.95 4.5 3.17" > l_depth
echo "4.991 3.549 3.074" > r_depth

# recording site labels
echo "A B C" > l_session_id
echo "A B C" > r_session_id

# ben-gun trajectories used during recordings
echo "A C P M" > l_traj
echo "A C P M" > r_traj

# final implantation trajectory depth
echo "1.511" > l_final_depth
echo "-0.517" > r_final_depth

# ben-gun trajectory selected for final implantation
echo "M" > l_final_traj
echo "M" > r_final_traj

# trajectories represented in intra-operative CT
echo "A C P" > l_ct_traj
echo "A C P" > r_ct_traj

# depth of electrodes in intra-operative CT
echo "0.546" > l_ct_depth
echo "-0.517" > r_ct_depth

echo "### demo_prep.sh - done"

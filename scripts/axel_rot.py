#!/usr/bin/env python

# last change (2017-11-05): improved readability

# axel_rot.py - adapted from Dr. Andrei Barborica's axel_rot.m,
# adapted from MATLAB's AxelRot.m function

# Generate roto-translation matrix for the rotation around an arbitrary line in 3D.
# The line need not pass through the origin. Optionally, also, apply this
# transformation to a list of 3D coordinates.

import numpy as np

def axel_rot(deg, u, x0):
    
    u = u / np.linalg.norm(u)

    # https://docs.scipy.org/doc/numpy-dev/user/numpy-for-matlab-users.html
    # important distinctions between python and MATLAB matrix math syntax ('*' is element-wise here)
    axis_shift = x0 - np.dot(x0, u) * u

    mat_shift = mkaff(np.identity(3), -1 * axis_shift)

    mat_rot = mkaff(R3d(deg, u), np.array([0, 0, 0]))

    mat_fin = np.dot(np.dot(np.linalg.inv(mat_shift), mat_rot), mat_shift)

    return mat_fin

def mkaff(rot, trans):
# Makes an affine transformation matrix, either in 2D or 3D.
# For 3D transformations, this has the form
#
#  M=[R,t;[0 0 0 1]]
#
# where R is a square matrix and t is a translation vector (column or row)
#
# When multiplied with vectors [x;y;z;1] it gives [x';y';z;1] which accomplishes the
# the corresponding affine transformation
#
# [x';y';z']=R*[x;y;z]+t

    nn = rot.shape[0]

    mat = np.identity(nn + 1)

    mat[0:(nn), 0:(nn)] = rot

    mat[0:(nn), nn] = trans
    
    return mat

def R3d(deg, u):
# R3D - 3D Rotation matrix counter-clockwise about an axis.
#
# R=R3d(deg,axis)
#
# deg: The counter-clockwise rotation about the axis in RADIANS (not degrees, as originally coded.
# axis: A 3-vector specifying the axis direction. Must be non-zero

    R = np.identity(3)
    u = u / np.linalg.norm(u)
    for i in [0, 1, 2]:
        v = R[:, i]
        R[:, i] = np.dot(v, np.cos(deg)) \
            + np.dot(np.cross(u, v), np.sin(deg)) \
            + np.dot(np.dot(np.dot(u, v), 1 - np.cos(deg)), u)

    return R

#!/usr/bin/env python

# last change (2017-11-28): added terminal output when '-dbs' option is used

# calc_depth_coords.py - queries and calculates the waypoint ACPC/MCP coordinates
# from a trajectory, taking into account the waypoint geometry and internal trajectory rotation
# (many thanks to FHC's Andrei Barborica for his MATLAB scripts)

import argparse as ap
import numpy as np
import os
from subprocess import call
import sys

def calc_depth_coords(track, track2, chunky_ct, tip, intraop_ct, intraop_ct_depth,
    samp_window, ppr_dir, depths, final_depth, no_final_depth_coord, trajs, image_traj,
    out_dir, macro, t1_warp, t1_warp_dir, dbs):

    print('\n---calc_depth_coords.py---\n')

    # check if there is an input track surface, and that it ends in *.clp.gii
    if track is None:
        print('ERROR: No input surface! (-track)')
        sys.exit()
    elif not track.endswith('.clp.gii'):
        print('ERROR: ' + track + ' does not end with *.clp.gii')
        sys.exit()

    # make the output directory if it doesn't exist
    if out_dir is None:
        out_dir = 'coords'

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    # are we looking at chunky CTs?
    if chunky_ct is True:
        print('# Averaging the resulting coordinates from axial and coronal chunky CTs...\n')
        if track2 is None:
            print('ERROR: You need two trajectories/tracks (-track, -track2) for chunky CT analysis!')
            sys.exit()
        elif not track2.endswith('.clp.gii'):
            print('ERROR: ' + track2 + ' does not end with *.clp.gii')
            sys.exit()

    # create an ID for output coordinates
    track_id = track.split('.')[0]
    if track2 is not None:
        track2_id = track2.split('.')[0]
    else:
        track2_id = ''

    ## electrode tip offset

    # post-op CTs or MRs (default) - 1.5mm offset of final stimulator trajectory
    # b/w tip of electrode, bottom of c0 both for medtronic 3387 and 3389

    # to use intra-op images, use '-intraop_ct' flag

    # to add a new tip offset (say 2mm), use '-tip 2'

    if tip is None:
        if intraop_ct is True:
            tip = 0
            print('# Using an intra-op CT - no tip offset will be used...')
            if intraop_ct_depth is None:
                print('ERROR: You need to enter in the trajectory depth')
                print('       (-intraop_ct_depth) at which the intraop CT was taken!')
                sys.exit()
            else:
                ct_depth = np.loadtxt(intraop_ct_depth)
                print('# Intra-op CT was taken at a depth of {} mm\n'.format(str(ct_depth)))
        else:
            # post-op imaging default
            print('# Assuming the electrode surface is from post-op imaging (1.5mm tip offset)...\n')
            tip = 1.5
            ct_depth = 0
    else:
        print('# Setting tip offset to {} above the bottom of the electrode of the trajectory'.format(tip))
        print('  (default = 1.5 mm)...\n')
        tip = np.float(tip)

    ## surface sampling window
    # sets the sampling window to include surface nodes above and below the trajectory depth

    # to set a new sampling window (let's say 2mm above and below the electrode), use '-samp_window 2'

    if samp_window is None:
        # default
        samp_window = 1
    else:
        print('# Setting sampling window to {} mm above and below recording depths'.format(samp_window))
        print('  (default = 1 mm)...\n')
        samp_window = np.float(samp_window)

    # check if dbs contact coordinates are specified
    # all depths are calculated b/w center of contact spans
    if dbs is not None:
        depths = 'dbs'
        print('# Reporting DBS contact coordinates for {} electrodes...\n'.format(dbs))

    # load the final depth value
    if final_depth is None:
        print('ERROR: Final electrode depth (-final_depth) not given for intra-')
        print('       or post-op electrode surface!')
        sys.exit()
    else:
       final_d2t = np.loadtxt(final_depth)

    # load the depth values
    if depths is None:
        print('ERROR: No input depth values! (-depths)')
        sys.exit()
    elif depths == 'dbs':
        if dbs == '3389':
            d2t = np.array([0.75, 2.75, 4.75, 6.75]) + final_d2t
        elif dbs == '3387':
            d2t = np.array([0.75, 3.75, 6.75, 9.75]) + final_d2t
    else:
        d2t = np.loadtxt(depths)

    # check if we are calculating final coordinates
    if no_final_depth_coord is False:
        d2t = np.append(d2t, 0)
    elif no_final_depth_coord is True:
        print('# Final depth will not calculated!\n')

    # check if we're calculating macro-electrode depths
    if macro is True:
        print('# Calculating macro-electrode depths...')

    go_macro = False
    
    # check if we need to apply intra-patient non-linear T1 warps
    if t1_warp is True:
    # load the t1_warp.py/auto_warp.py transformation matrices
        if t1_warp_dir is None:
            t1_warp_dir = 't1_warp'
        else:
            print('# Loading intra-patients warps from {}...\n'.format(t1_warp_dir))
             
        if os.path.exists(os.path.join(t1_warp_dir,'anat.un.aff.qw_WARP.nii')) \
            and os.path.exists(os.path.join(t1_warp_dir, 'anat.un.aff.Xat.1D')):

            xfm = '{} {}'.format(os.path.join(t1_warp_dir, 'anat.un.aff.qw_WARP.nii'),
                os.path.join(t1_warp_dir, 'anat.un.aff.Xat.1D'))
        else:
            print(' ERROR: transformation matrices/atlas directory not found!')
            print('(-t1_warp; {}anat.un.aff.qw_WARP.nii, {}anat.un.aff.Xat.1D)'.format(t1_warp_dir,t1_warp_dir))
            sys.exit()
    else:
        xfm = None

    # load the proper trajectory from the ppr_directory
    # L/R test the accompanying track's nifti center-of-mass, specifically the x-coordinate
    com = np.loadtxt(track_id + '.com.1D')
    if com[0] > 0: #positive, meaning left
        l_r = 'l'
        print('# Loading a left brain trajectory...')
    elif com[0] < 0: #negative, meaning right
        l_r = 'r'
        print('# Loading a right brain trajectory...')
    else:
        print('ERROR: Center-of-mass L/R trajectory test failed! (check *track*.com.1D)')
        sys.exit()

    if ppr_dir is None:
        print('ERROR: No input ppr directory! (-ppr_dir)')
        sys.exit()
    else:
        # based on the l/r distinction, let's load some trajectory target and entry coordinates
        tp = np.loadtxt(os.path.join(ppr_dir, (l_r + '_traj_target')))
        ep = np.loadtxt(os.path.join(ppr_dir, (l_r + '_traj_entry')))

        # Load transformation matrix for mapping CT space to anatomical space (TWCT_i)
        # and mapping to MCP alignment (TMCP)
        TWCT_i = np.loadtxt(os.path.join(ppr_dir, 'TWCT_i'))
        TWCT_i = TWCT_i.reshape(4, 4)
        TMCP = np.loadtxt(os.path.join(ppr_dir, 'TMCP'))
        TMCP = TMCP.reshape(4, 4)

    # let's now transform the trajectory coordinates into MCP space

    #add a 1 to trajectory coordinates to make them homogenous coordinates
    ep_h = np.array(np.append(ep, 1))
    tp_h = np.array(np.append(tp, 1))

    # Calculate MCP coordinates of the trajectory by multiplying with the
    # waypoint transformation matrices
    epMCP = np.dot(TMCP, np.dot(TWCT_i, ep_h))
    tpMCP = np.dot(TMCP, np.dot(TWCT_i, tp_h))

    # From there, figure out the trajectory angle geometry
    # Rotation around the X axis
    rotX = np.arctan2(epMCP[1] - tpMCP[1],
        np.linalg.norm(np.array([[epMCP[0] - tpMCP[0]], [epMCP[2] - tpMCP[2]]])))
    # Rotation angle around Y
    rotY = np.arctan2(epMCP[0] - tpMCP[0], epMCP[2] - tpMCP[2])

    # Create some matrices
    Rx = [ [1, 0, 0, 0],
        [0, np.cos(rotX), np.sin(rotX), 0],
        [0, -1 * np.sin(rotX), np.cos(rotX), 0],
        [0, 0, 0, 1] ]

    Ry = [ [np.cos(rotY), 0, np.sin(rotY), 0],
        [0, 1, 0, 0],
        [-1 * np.sin(rotY), 0, np.cos(rotY), 0],
        [0, 0, 0, 1] ]

    # See original PlatformTracks.m for RotZ and translations
    # (not used here because rotZ was always set to 0 and not used
    # translations not used here as they are based on ideal trajectory geometry,
    # not where the electrodes and their artifacts actually show up)

    # I also do not assign the depth value to the z-coordinate of the ben-gun transformations,
    # as this again assumes geometry over actual location - however, it would be useful to
    # compare predicted depth coordinates vs actual depth coordinates using this method.

    # now load the trajectory list files (e.g. a,c,p)
    if image_traj is None:
        print('ERROR: No reference trajectory specified! (-image_traj)')
        sys.exit()
    else:
        ref_traj = verbatim_lines_traj(image_traj)

    if trajs is None:
        print('ERROR: No recording trajectories specified! (-trajs)')
        sys.exit()
    else:
        calc_traj = verbatim_lines_traj(trajs)

    # if the number of ref_ and calc_trajectories do not match for intra-op CTs, send a warning
    if intraop_ct is True:
        if np.shape(ref_traj)[0] is not np.shape(calc_traj)[0]:
            print('# WARNING: The number of reference and coordinate trajectories do not match!!!\n')
            print('# Assuming this is an intra-op CT, was an electrode added/removed before/after')
            print('  the CT was acquired?\n')
            print('# Reference trajectories (-image_traj): ' + str(ref_traj))
            print('# Coordinate trajectories (-trajs): ' + str(calc_traj))

    # assign the reference trajectory to the one displayed in the image
    ref_traj_d = np.zeros((np.shape(ref_traj)[0],4))
    for nt, t in enumerate(ref_traj):
        ref_traj_d[nt, :] = tracksMCP_search(t, l_r)

    # create an average reference trajectory if there is more than one trajectory
    # in the image (where will the surface center-of-mass coordinates be?)
    if np.shape(ref_traj)[0] > 1:
        ref_traj_d_avg = np.array([np.sum(ref_traj_d[:, 0]) / 2, np.sum(ref_traj_d[:, 1]) / 2, 0, 1])
    else:
        # otherwise, just use the single trajectory
        ref_traj_d_avg = ref_traj_d

    # for each trajectory to calculate coordinates for,
    # find the distance between the reference trajectory and the trajectory to consider
    calc_traj_d = np.zeros((np.shape(calc_traj)[0], 4))
    for nt, t in enumerate(calc_traj):
        calc_traj_d[nt, :] = tracksMCP_search(t, l_r) - ref_traj_d_avg

    # keep the coordinates homogenous
    calc_traj_d[:, 3] = 1

    # now, for each depth value,
    # let's calculate the micro (and macro, optionally)
    # coordinates for each trajectory

    # intraop
    if intraop_ct is True:
        for d in d2t:
            # make sure that the recording depth is above where the CT was taken -
            # you can't calculate a coordinate based on an artifact that doesn't exist
            # (we could theoretically project...but that is not supported at this time)
            if d >= ct_depth:
                calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                    chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                if macro is True:
                    go_macro = True
                    calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                        chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                    go_macro = False
            else:
                # 0 is the depth code for final depth, let's make sure the true final depth
                # value is not below the ct_depth
                if d == 0:
                    if final_d2t >= ct_depth:
                        calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track,
                            samp_window, chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d,
                            go_macro, xfm)
                        if macro is True:
                            go_macro = True
                            calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track,
                                samp_window, chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d,
                                go_macro, xfm)
                            go_macro = False
                    else:
                        print('# Final depth {} is lower than the ct depth ({})!'.format(str(d), str(ct_depth)))
                        print('  Final depth can not be calculated.\n')
                else:
                    print('# Depth {} is lower than the ct depth ({})!'.format(str(d), str(ct_depth)))
                    print('  Depth can not be calculated.\n')

    # postop
    else:
        for d in d2t:
            if d >= final_d2t:
                calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                    chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                if macro is True:
                    go_macro = True
                    calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                        chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                    go_macro = False
            elif d == 0:
                calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                    chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                if macro is True:
                    go_macro = True
                    calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
                        chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm)
                    go_macro = False
            else:
                print('# Depth {} is lower than the final depth ()!'.format(str(d), str(final_d2t)))
                print('  Depth can not be calculated.\n')
        
def calc_coords(d, intraop_ct, ct_depth, tip, final_d2t, track_id, out_dir, track, samp_window,
    chunky_ct, track2_id, track2, calc_traj, Ry, Rx, calc_traj_d, go_macro, xfm):

    # calculate a depth-specific offset based on the tip length, final implantation depth,
    # and intraop_ct depth values
    if intraop_ct is True:
        # intraop
        d_offset = (-1 * ct_depth) + d
    else:
        # postop
        # final depth
        if d == 0:
            d_offset = tip
        # recording depths
        else:
            d_offset = (tip - final_d2t) + d

    # 3mm above target offset for macro electrode
    if go_macro is True:
        d_offset += 3

    # create a filename prefix based on output directory and depth value
    if go_macro is True:
        fn_prefix = track_id + '_macro_' + str(d)
    else:
        fn_prefix = track_id + '_' + str(d)

    prefix = os.path.join(out_dir, fn_prefix)

    # create a surface center-of-mass coordinate for each depth, save to disk
    with open(os.devnull, 'w') as fnull:
        call(['SurfClust',
            '-i', track,
            '-input', track_id + '.clp.pcdepth.1D.dset', '2',
            '-rmm', '10',
            '-thresh_col', '2',
            '-in_range', str(d_offset - samp_window), str(d_offset + samp_window),
            '-prefix',prefix],
            stdout=fnull,
            stderr=fnull)

    surf_coord_fn = str(prefix + '_surf.coord')
    if os.path.isfile(surf_coord_fn):
        print(surf_coord_fn)
        print('# File already exists, will not overwrite.')
    else:
        with open(surf_coord_fn, 'w') as f:
            call(['1dcat',
                str(prefix + "_ClstTable_r10.0.1D'{0}[20,21,22]'")],
                stdout=f)

    # chunky ct double-calculation
    if chunky_ct is True:
        if go_macro is True:
            fn_prefix2 = track2_id + '_macro_' + str(d)
        else:
            fn_prefix2 = track2_id + '_' + str(d)

        prefix2 = os.path.join(out_dir, fn_prefix2)

        with open(os.devnull, 'w') as fnull:
            call(['SurfClust',
                '-i', track2,
                '-input', track2_id + '.clp.pcdepth.1D.dset', '2',
                '-rmm', '10',
                '-thresh_col', '2',
                '-in_range', str(d_offset - samp_window), str(d_offset + samp_window),
                '-prefix',prefix2],
                stdout=fnull,
                stderr=fnull)

        surf_coord2_fn = str(prefix2 + '_surf.coord')
        if os.path.isfile(surf_coord2_fn):
            print(surf_coord2_fn)
            print('#File already exists, will not overwrite.')
        else:
            with open(surf_coord2_fn, 'w') as f:
                call(['1dcat',
                    str(prefix2 + "_ClstTable_r10.0.1D'{0}[20,21,22]'")],
                    stdout=f)

    # now let's re-load the surface coordinate into python for applying ben-gun transformations
    surf_coord = np.loadtxt(surf_coord_fn)

    # chunky ct double-load and average
    if chunky_ct is True:
        surf_coord2 = np.loadtxt(surf_coord2_fn)
        surf_coord = (surf_coord + surf_coord2) / 2

    # for each of our ben-gun transformations (ref->A, ref->C, ref->P)
    for nt, t in enumerate(calc_traj):
        # apply the trajectory rotations to the ideal ben-gun shifts
        calc_traj_coord = np.dot(Ry, np.dot(Rx, calc_traj_d[nt, :]))

        # apply this modified rotation to the surface coordinate
        surf_d = calc_traj_coord[0:3] + surf_coord

        # now save to disk
        calc_traj_coord_fn = fn_prefix + '_' + t.lower() + '_track.coord'
        calc_traj_coord_out = str(surf_d[0:3])[1:-1]
        prettysave(calc_traj_coord_out, calc_traj_coord_fn, out_dir)

        # at the end, use an intra-patient nonlinear T1 warp if indicated
        if xfm is not None:
            xfm_coord_fn = os.path.join(out_dir, (calc_traj_coord_fn + '.t1warp.coord'))
            with open(xfm_coord_fn, 'w') as f:
                call(['3dNwarpXYZ',
                    '-nwarp', xfm,
                    '-iwarp', os.path.join(out_dir, calc_traj_coord_fn)],
                    stdout=f)
                
            # clean up old non-warped coordinate file?
            # call(['rm',os.path.join(out_dir, calc_traj_coord_fn)])

# saves the output to the output directory
def prettysave(item, fn, out_dir):
    dir_path = os.path.join(out_dir, fn)
    if not os.path.exists(dir_path):
        with open(dir_path, 'w') as out:
            out.write("{}\n".format(item))
    else:
        print(fn)
        print('#File already exists, will not overwrite.\n')

def tracksMCP_search(t, l_r):

    t = t.lower()

    # Define the coordinates of the tracks in a Ben-gun configuration, in both 
    # ACEG and BDFH configuration, located in the axial plane, centered on
    # the MCP (at target).

    # Columns correspond to tracks: center (C), posterior (P), left (L),
    # anterior (A), right (R), posterior-left (PL), anterior-left (AL),
    # anterior-right (AR), posterior-right (PR)

    # PML change: ANTERIOR is now POSTERIOR, and vice-versa (currently anterior = [0,-2,0,1];
    # previously posterior = [0,-2,0,1])
    # Above list of columns/tracks has been updated to reflect this
    tracksMCP = np.array([ [0, 0, 0, 1],
        [0, 2, 0, 1],
        [2, 0, 0, 1],
        [0, -2, 0, 1],
        [-2, 0, 0, 1],
        [np.sqrt(2), np.sqrt(2), 0, 1],
        [np.sqrt(2), -1 * np.sqrt(2), 0, 1],
        [-1 * np.sqrt(2), -1 * np.sqrt(2), 0, 1],
        [-1 * np.sqrt(2), np.sqrt(2), 0, 1] ]).transpose()

    #cardinal arrangement (classic ben-gun)
    if t == 'a':
        t_d = tracksMCP[:, 3]
    elif t == 'p':
        t_d = tracksMCP[:, 1]
    elif t == 'c':
        t_d = tracksMCP[:, 0]
    elif t == 'l':
        if l_r == 'l':
            t_d = tracksMCP[:, 2]
        elif l_r == 'r':
            t_d = tracksMCP[:, 4]
    elif t == 'm':
        if l_r == 'l':
            t_d = tracksMCP[:, 4]
        elif l_r == 'r':
            t_d = tracksMCP[:, 2]

    #clockwise 45 degree rotation
    if t == 'cwa':
        t_d = tracksMCP[:, 7]
    elif t == 'cwp':
        t_d = tracksMCP[:, 5]
    elif t == 'cwc':
        t_d = tracksMCP[:, 0]
    elif t == 'cwl':
        if l_r == 'l':
            t_d = tracksMCP[:, 6]
        elif l_r == 'r':
            t_d = tracksMCP[:, 8]
    elif t == 'cwm':
        if l_r == 'l':
            t_d = tracksMCP[:, 8]
        elif l_r == 'r':
            t_d = tracksMCP[:, 6]

    #counter-clockwise 45 degree rotation
    if t == 'ccwa':
        t_d = tracksMCP[:, 6]
    elif t == 'ccwp':
        t_d = tracksMCP[:, 8]
    elif t == 'ccwc':
        t_d = tracksMCP[:, 0]
    elif t == 'ccwl':
        if l_r == 'l':
            t_d = tracksMCP[:, 5]
        elif l_r == 'r':
            t_d = tracksMCP[:, 7]
    elif t == 'ccwm':
        if l_r == 'l':
            t_d = tracksMCP[:, 7]
        elif l_r == 'r':
            t_d = tracksMCP[:, 5]
    
    return t_d

def verbatim_lines_traj(f):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]
        traj = str(lines[0])
        verbatim_traj = traj.split()

    return verbatim_traj

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='#This script queries and calculates the waypoint \
        ACPC/MCP coordinates from a CT-derived trajectory surface, taking into account the waypoint \
        geometry and internal trajectory rotation')

    parser.add_argument('-track', action='store',
        help='the clipped gifti surface (*clp.gii) of an electrode track')

    parser.add_argument('-track2', action='store',
        help='a duplicate clipped gifti surface (*clp.gii) of an electrode track (-chunky_ct also required)')

    parser.add_argument('-chunky_ct', action='store_true', default=False,
        help='flag that averages the resulting surface coordinate of the axial and coronal chunky CTs')

    parser.add_argument('-tip', action='store',
        help='enter a tip-to-recording-site offset value in mm (default: 1.5 mm)')

    parser.add_argument('-intraop_ct', action='store_true', default=False,
        help='flag that sets tip offset to 0mm')

    parser.add_argument('-intraop_ct_depth', action='store',
        help='text file containing the depth at which the intraop_ct was taken')

    parser.add_argument('-samp_window', action='store',
        help='enter a new surface sampling window value in mm (default: 1 mm) ')

    parser.add_argument('-ppr_dir', action='store',
        help='the location of ppr directory generated by read_ppr.py')

    parser.add_argument('-depths', action='store',
        help='text file containing trajectory depths to be calculated into coordinates')

    parser.add_argument('-final_depth', action='store',
        help='text file containing the trajectory depth where the electrode was implanted (postop images only)')

    parser.add_argument('-no_final_depth_coord', action='store_true', default=False,
        help='flag that prevents the final depth from being calculated')

    parser.add_argument('-trajs', action='store',
        help='text file containing which trajectories (e.g. A, C, P) to calculate coordinates for')

    parser.add_argument('-image_traj', action='store',
        help='text file containing which trajectory/trajectories (e.g. A, C, P) the -track \
            gifti surface represents')

    parser.add_argument('-out_dir', action='store',
        help='specify the output directory for coordinate text files (default = coords/)')

    parser.add_argument('-macro', action='store_true', default=False,
        help='flag that also calculates/outputs the location of the macro-electrode for each \
            micro-electrode recording site')

    parser.add_argument('-t1_warp', action='store_true', default=False,
        help='flag that uses intra-patient T1 warps for calculating coordinates from post-op MRIs')
    
    parser.add_argument('-t1_warp_dir', action='store',
        help='directory containing intra-patient non-linear warps')

    parser.add_argument('-dbs', action ='store',
        help='flag to pre-load medtronic DBS contact coordinates (options: 3387, 3389)')

    args = parser.parse_args()

    calc_depth_coords(args.track, args.track2, args.chunky_ct, args.tip, args.intraop_ct,
        args.intraop_ct_depth, args.samp_window, args.ppr_dir, args.depths, args.final_depth,
        args.no_final_depth_coord, args.trajs, args.image_traj, args.out_dir, args.macro, args.t1_warp,
        args.t1_warp_dir, args.dbs)

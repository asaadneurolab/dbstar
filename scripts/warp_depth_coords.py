#!/usr/bin/env python

# last change (2017-11-16): suppressed AFNI output

# warp_depth_coords.py - applies the atlas transformation matrices
# to patient-specific coordinates, bringing them to to a common atlas
# (typically TT_N27), generate 1mm spheres, and test their atlas location

import argparse as ap
import numpy as np
import os
from subprocess import call
import sys

def warp_depth_coords(coord_dir, atlas_dir, test_atlas, make_spheres,
    sphere_vals, rad):

    print('\n---warp_depth_coords.py---\n')

    # define coordinate directory if not specified
    if coord_dir is None:
        coord_dir = 'coords'

    # check if a new test atlas has been specified
    if test_atlas is None:
        test_atlas = 'TT_Daemon'

    # define atlas directory if not specified
    if atlas_dir is None:
        atlas_dir = 'tt_warp'

    # check if spheres will be generated
    if make_spheres is True:
        print('# Generating spheres for atlas coordinate visualization...\n')
        if sphere_vals is not None:
            print('# Loading physiology values for sphere generation...\n')
            d2t = np.loadtxt(sphere_vals[0])
            track = sphere_vals[1].lower()
            p2t = np.loadtxt(sphere_vals[2])

        # load the sphere radius - if not defined, set to 1 mm
        if rad is None:
            rad = '1'

    # check that the atlas_align.py reference volume is present
    if os.path.exists(os.path.join(atlas_dir, 'base.nii')):
        ref_vol = os.path.join(atlas_dir, 'base.nii')
    else:
        print('ERROR: atlas reference volume not found! \
            (-atlas_dir; atlas_dir/base.nii)')
        sys.exit()

    # load the atlas_align.py/auto_warp.py transformation matrices
    if os.path.exists(os.path.join(atlas_dir, 'anat.un.aff.qw_WARP.nii')) \
        and os.path.exists(os.path.join(atlas_dir, 'anat.un.aff.Xat.1D')):
        xfm = '{} {}'.format(os.path.join(atlas_dir, 'anat.un.aff.qw_WARP.nii'),
            os.path.join(atlas_dir, 'anat.un.aff.Xat.1D'))
    else:
        print('ERROR: transformation matrices/atlas directory not found! \
            (-atlas_dir; atlas_dir/anat.un.aff.qw_WARP.nii, atlas_dir/anat.un.aff.Xat.1D)')
        sys.exit() 

    # let's warp each *.coord in the coordinate directory,
    # generate a 1mm radius sphere at each warped coordinate
    print('# Warping coordinates to the atlas and testing their location...\n')
    for file in os.listdir(coord_dir):
        if file.endswith('.coord'):
            coord_fn = os.path.join(coord_dir, file)

            # check if the coordinate file is empty, if not load it
            if os.stat(coord_fn).st_size == 0:
                print('# ' + file + ' is empty...skipping')
            else:

                # apply warp (should you ever need to reverse this warp
                # (heaven forbid) just apply the same command to the
                # atlas coordinate sans '-iwarp')
                warp_fn = os.path.join(atlas_dir, (file[0:-6] + '.iwarp.coord'))
                if os.path.isfile(warp_fn) is True:
                    print(warp_fn)
                    print('# File already exists, will not overwrite.')
                else:
                    with open(warp_fn, 'w') as f:
                        with open(os.devnull, 'w') as fnull:
                            call(['3dNwarpXYZ',
                                '-nwarp', xfm,
                                '-iwarp',
                                coord_fn],
                                stdout=f, stderr=fnull)

                # test warped coordinate
                tt_test_fn = warp_fn + '.atlas.test'
                if os.path.isfile(tt_test_fn) is True:
                    print(tt_test_fn)
                    print('# File already exists, will not overwrite.')
                else:
                    with open(tt_test_fn, 'w') as f:
                        call(['whereami',
                            '-coord_file', warp_fn,
                            '-atlas', test_atlas],
                            stdout=f)

                # sphere generation (now 1mm (srad))
                if make_spheres is True:
                    if sphere_vals is None:
                        with open(os.devnull, 'w') as fnull:
                            call(['3dUndump',
                            '-master', ref_vol,
                            '-xyz',
                            '-dval', '1',
                            '-srad', rad,
                            '-orient', 'RAI',
                            '-prefix', warp_fn + '.sphere.nii.gz',
                            warp_fn], stdout=fnull, stderr=fnull)
                    else:
                        if warp_fn.split('_')[5] == track:
                            if float(warp_fn.split('_')[4]) in d2t:
                                d2t_i = np.where(d2t==float(warp_fn.split('_')[4]))[0][0]
                                p2t_val = p2t[d2t_i]
                                call(['3dUndump',
                                    '-master', ref_vol,
                                    '-xyz',
                                    '-dval', str(p2t_val),
                                    '-datum', 'float',
                                    '-srad', rad,
                                    '-orient', 'RAI',
                                    '-prefix', warp_fn + '.sphere.phys.nii.gz',
                                    warp_fn])
                                

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script applies non-linear \
        transformation matrices from atlas_align.py to patient coordinates \
        to bring them to a common atlas space (e.g. TT_N27), generate 1mm \
        spheres, and test their atlas location')

    parser.add_argument('-coord_dir', action='store',
        help='directory containing coordinates to test (typically generated \
            by calc_depth_coords.py (default: coords)')

    parser.add_argument('-atlas_dir', action='store',
        help='output directory generated by atlas_align.py containing \
            non-linear warp volumes and transforms - also the output \
            directory for warped coordinates (default: tt_warp)')

    parser.add_argument('-test_atlas', action='store',
        help='specify a different atlas for AFNI/whereami to check \
            coordinate location (default: TT_Daemon)')

    parser.add_argument('-make_spheres', action='store_true', default=False,
        help='flag to generate atlas coordinate 1mm-radius (default) \
            spheres for each recording location - warning: may take up a \
            lot of disk space')

    parser.add_argument('-sphere_vals', action='store', nargs=3,
        help='values for sphere generation - first is depth values, second \
            is trajectory (A,C,P), third is values to plot')

    parser.add_argument('-rad', action='store',
        help='value for generated sphere radius in mm (default: 1)')

    args = parser.parse_args()

    warp_depth_coords(args.coord_dir, args.atlas_dir, args.test_atlas,
        args.make_spheres, args.sphere_vals, args.rad)

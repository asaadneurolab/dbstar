#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes

# t1_acpc_seg.py - freesurfer-segments T1 MR images in the waypoint ACPC/MCP coordinate space

import argparse as ap
import os
from subprocess import call
import sys

def t1_acpc_seg(fn):
    print('\n---t1_acpc_seg.py---\n')

    # check inputs
    if fn is None:
        print('ERROR: No input volume! (-t1_vol)')
        sys.exit()

    print('# Performing freesurfer segmentation...\n')

    fn_u = fn.split('.')[0] + '_u.nii.gz'
    fn_u_id = fn_u.split('.')[0]

    # intensity normalize the image with 3dUnifize
    call(['3dUnifize',
        '-prefix', fn_u,
        fn])

    # freesurfer-segment the resulting image with default 'recon-all' pipeline (24-48 hours/patient)
    call(['recon-all',
        '-i', fn_u,
        '-s', fn_u_id,
        '-sd', './',
        '-all',
        '-parallel'])

    # jump into the resulting directory to SUMA-reconstruct the freesurfer surfaces for later
    os.chdir(fn_u_id + '/')

    # SUMA-reconstruct all the freesurfer volumes
    call(['@SUMA_Make_Spec_FS',
        '-GIFTI',
        '-sid', fn_u_id])

    os.chdir('..')

    # copy the native freesurfer segmentation to the patient directory for native coordinate testing
    call(['cp', os.path.join(fn_u_id, 'SUMA/aparc.a2009s+aseg_REN_gm.nii.gz'), './'])

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script freesurfer-segments T1 MR images in the \
        waypoint ACPC/MCP coordinate space')

    parser.add_argument('-t1_vol', action='store',
        help='a T1 nifti volume brought to the ACPC/MCP space by waypoint_convert.py \
            (e.g. t1_pre_refit_mcp.nii)')

    args = parser.parse_args()

    t1_acpc_seg(args.t1_vol)

#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes

# intensity_invert.py - inverts the intensities of T1 volumes for CT-like segmentation

import argparse as ap
import numpy as np
from subprocess import call
import sys
import os

def intensity_invert(fn, skull_strip, lr_flip):
    print('\n---intensity_invert.py---\n')

    # check inputs
    if os.path.isfile(fn) is False:
        print('ERROR: input volume does not exist (-t1_vol)!')
        sys.exit()

    if fn is None:
        print('ERROR: No input volume (-t1_vol)!')
        sys.exit()

    # skull-strip or auto-mask
    if skull_strip is True:
        print('# Skull-stripping {}...\n'.format(fn))
        # skull-strip the image for maximum intensity value calculation
        preproc_fn = fn.split('.')[0] + '_brain.nii.gz'

        call(['3dSkullStrip',
            '-input', fn,
            '-prefix', preproc_fn])
    else:
        print('# Auto-masking {}...\n'.format(fn))
        # automask the image for maximum intensity value calculation
        preproc_fn = fn.split('.')[0] + '_amd.nii.gz'

        call(['3dAutomask',
            '-apply_prefix', preproc_fn,
            '-dilate', '3',
            fn])
        
    # calculate the maximum intensity value, save to disk
    max_fn = preproc_fn.split('.')[0] + '_max'
    with open(max_fn, 'w') as f:
        call(['3dBrickStat',
            '-max', preproc_fn],
            stdout=f)

    # load the maximum value back in
    vox_max = str(np.loadtxt(max_fn))

    # now invert the image using this maximum intensity value using 3dcalc
    print('# Intensity-inverting {}...\n'.format(preproc_fn))
    inv_fn = preproc_fn.split('.')[0] + '_inv.nii.gz'

    call(['3dcalc',
        '-a', preproc_fn,
        '-expr', 'step(a)*({}-a)'.format(vox_max),
        '-prefix',inv_fn])

    # clean up maximum voxel value
    call(['rm',max_fn])

    # left-right flip the image (optional)
    if lr_flip is True:
        print('# Left-right flipping the volume...QUALITY-CHECK the results with the original volume!\n')
        flip_fn = inv_fn.split('.')[0] + '_LRflip.nii.gz'

        call(['3dLRflip',
            '-prefix', flip_fn,
            inv_fn])

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script inverts the intensities of T1 volumes \
        for CT-like segmentation')

    parser.add_argument('-t1_vol', action='store',
        help='a T1 nifti volume brought to the ACPC/MCP space by waypoint_convert.py \
            (e.g. t1_pre_refit_mcp.nii.gz), or a mipav-processed volume (e.g. t1_midsag_acpc.nii)')

    parser.add_argument('-skull_strip', action='store_true', default=False,
        help='flag which skull-strips the input volume to only perform the intensity inversion \
            on the resulting brain parenchyma - best option for CT-like segmentation')

    parser.add_argument('-lr_flip', action='store_true', default=False,
        help='flag which left-right flips the volume (USE ONLY AFTER MIPAV PROCESSING)')

    args = parser.parse_args()

    intensity_invert(args.t1_vol, args.skull_strip, args.lr_flip)

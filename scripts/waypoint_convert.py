#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz files,
#
#                           replaced image-specific commands with common functions,
#
#                           changed order of script to perform all steps one image at a time,

# waypoint_convert.py - script that pre-processes images,
# bringing them all to the same waypoint ACPC/MCP coordinate space

import alignMCP
import argparse as ap
import numpy as np
import os
from subprocess import call

def waypoint_convert(ppr_dir, preop_ct, preop_t1, extra1, extra2, intraop_ct, xfm_qc,
                    save_refit, image_order):

    print('\n---waypoint_convert.py---\n')

    print('## Applying waypoint plan geometry to images...\n')

    # default order of images (I1-preop_ct, I2-preop_t1, I3-extra1, I4-extra2)
    if image_order is None:
        image_order = ['I1', 'I2', 'I3', 'I4']

    # load some anatomical information from the ppr output
    ac = np.loadtxt(os.path.join(ppr_dir, 'ac_coord'))
    pc = np.loadtxt(os.path.join(ppr_dir, 'pc_coord'))
    midsag = np.loadtxt(os.path.join(ppr_dir, 'midsag_coord'))

    # load image-specific waypoint info
    ct_info = np.loadtxt(os.path.join(ppr_dir, (image_order[0] + '_image_info')))
    t1_info = np.loadtxt(os.path.join(ppr_dir, (image_order[1] + '_image_info')))

    if extra1 is not None:
        ex1_info = np.loadtxt(os.path.join(ppr_dir, (image_order[2] + '_image_info')))
        ex1_intraop_ct = False
        if intraop_ct is not None:
            if intraop_ct == 'extra1':
                ex1_intraop_ct = True
    if extra2 is not None:
        ex2_info = np.loadtxt(os.path.join(ppr_dir, (image_order[3] + '_image_info')))
        ex2_intraop_ct = False
        if intraop_ct is not None:
            if intraop_ct == 'extra2':
                ex2_intraop_ct = True

    # Calculate transformation matrix for mapping anatomical space to CT space,
    # based on the AC, PC, and midsag coordinates
    TWCT = np.array([[-1, 0, 0, (ct_info[0] - 1) * ct_info[3] / 2],
        [0, 1, 0, (ct_info[1] - 1) * ct_info[4] / 2],
        [0, 0, -1, (ct_info[2] - 1) * ct_info[5] / 2],
        [0, 0, 0, 1]])

    # now invert it
    TWCT_i = np.linalg.inv(TWCT)

    # save the TWCT and TWCT_i matrices
    TWCT_out = '{} {} {} {}'.format(str(TWCT[0, :])[1:-1], str(TWCT[1, :])[1:-1],
                str(TWCT[2, :])[1:-1], str(TWCT[3, :])[1:-1])
    TWCT_i_out = '{} {} {} {}'.format(str(TWCT_i[0, :])[1:-1], str(TWCT_i[1, :])[1:-1],
                str(TWCT_i[2, :])[1:-1], str(TWCT_i[3, :])[1:-1])

    prettysave(TWCT_out, 'TWCT', ppr_dir)
    prettysave(TWCT_i_out, 'TWCT_i', ppr_dir)

    # bring AC, PC, and midsag to the CT image
    ac_h = np.array(np.append(ac, 1))
    pc_h = np.array(np.append(pc, 1))
    mp_h = np.array(np.append(midsag, 1))

    ct_ac = np.dot(TWCT_i, ac_h)[0:3]
    ct_pc = np.dot(TWCT_i, pc_h)[0:3]
    ct_mp = np.dot(TWCT_i, mp_h)[0:3]

    # now let's create a CT -> ACPC matrix using the AC, PC,
    # and midsag coordinates in the original CT volume

    # note the differing order of PC and AC -
    # this accounts for the different orientation/sign of the native CT y-axis
    TMCP = alignMCP.alignMCP(ct_pc, ct_ac, ct_mp)
    # now invert it
    TMCP_i = np.linalg.inv(TMCP)

    # save the TMCP and TMCP_i matrices
    TMCP_out = '{} {} {} {}'.format(str(TMCP[0, :])[1:-1], str(TMCP[1, :])[1:-1],
                str(TMCP[2, :])[1:-1], str(TMCP[3, :])[1:-1])
    TMCP_i_out = '{} {} {} {}'.format(str(TMCP_i[0, :])[1:-1], str(TMCP_i[1, :])[1:-1],
                str(TMCP_i[2, :])[1:-1], str(TMCP_i[3, :])[1:-1])

    prettysave(TMCP_out, 'TMCP', ppr_dir)
    prettysave(TMCP_i_out, 'TMCP_i', ppr_dir)

    # refit and resample CT
    ct_refit = refit(preop_ct, ct_info, False)
    # bring pre-op CT to ACPC/MCP space (only use the TMCP_i transform)
    combine_xfm(TMCP_i, None, image_order[0], ppr_dir, ct_refit, True)

    # refit and resample pre-op T1
    t1_refit = refit(preop_t1, t1_info, False)
    # load waypoint plan registration for pre-op T1
    # use -xfm_qc to quality-check these registrations
    t1_xfm = load_xfm(image_order[1], t1_refit, ppr_dir, xfm_qc)
    # bring pre-op T1 to ACPC/MCP space (combine waypoint xfm, ACPC/MCP xfm)
    combine_xfm(TMCP_i, t1_xfm, image_order[1], ppr_dir, t1_refit, False)

    # replicate pre-op T1 steps for extra1/2 images, if present
    if extra1 is not None:
        ex1_refit = refit(extra1, ex1_info, ex1_intraop_ct)
        ex1_xfm = load_xfm(image_order[2], ex1_refit, ppr_dir, xfm_qc)
        combine_xfm(TMCP_i, ex1_xfm, image_order[2], ppr_dir, ex1_refit, False)
    if extra2 is not None:
        ex2_refit = refit(extra2, ex2_info, ex2_intraop_ct)
        ex2_xfm = load_xfm(image_order[3], ex2_refit, ppr_dir, xfm_qc)
        combine_xfm(TMCP_i, ex2_xfm, image_order[3], ppr_dir, ex2_refit, False)

    # unless otherwise indicated, remove intermediate volumes (*refit.nii.gz) to save disk space
    if save_refit is True or xfm_qc is True:
        print('## Saving intermediate volumes (*refit.nii.gz)...\n')
    else:
        print('## Removing intermediate volumes (*refit.nii.gz)...\n')
        call(['rm', ct_refit])
        call(['rm', t1_refit])
        if extra1 is not None:
            call(['rm', ex1_refit])
        if extra2 is not None:
            call(['rm', ex2_refit])

    print('## Waypoint -> AFNI conversion complete...')
    print('## Check the *refit_mcp.nii.gz volumes to make sure all images are registered and MC is <0,0,0>')

def refit(vol, info, intraop_ct):

    print('# Refitting/resampling {}...'.format(vol))
    vol_refit = vol.split('.')[0] + '_refit.nii.gz'

    with open(os.devnull, 'w') as fnull:

        # make a copy of ct_pre and t1_pre (and extra(1/2)) prior to coordinate refitting
        call(['3dcopy',
            vol, vol_refit],
            stdout=fnull, stderr=fnull)

        # now let's refit those images to set the origin as the center
        # for each dimension, origin = [(number of voxels - 1) * voxel size] / 2)
        if intraop_ct is True:
            call(['3drefit',
                '-orient', 'LPI',
                '-xorigin', str((info[0] - 1) * info[3] / 2),
                '-yorigin', str((info[1] - 1) * info[4] / 2),
                '-zorigin', str((info[2] - 1) * info[5] / 2),
                vol_refit],
                stdout=fnull, stderr=fnull)
        else:
            call(['3drefit',
                '-xorigin', str((info[0] - 1) * info[3] / 2),
                '-yorigin', str((info[1] - 1) * info[4] / 2),
                '-zorigin', str((info[2] - 1) * info[5] / 2),
                vol_refit],
                stdout=fnull, stderr=fnull)

        call(['3dresample',
            '-orient', 'RPI',
            '-prefix', vol_refit,
            '-input', vol_refit,
            '-overwrite'],
            stdout=fnull, stderr=fnull)

    return vol_refit

def load_xfm(vol_id, vol_refit, ppr_dir, xfm_qc):

    # load waypoint's transformation matrices for each volume
    vol_xfm_fn = (vol_id + '_transform.1D')
    vol_xfm = np.loadtxt(os.path.join(ppr_dir, vol_xfm_fn))
    vol_xfm = vol_xfm.reshape(4, 4)
    vol_xfm_al_fn = vol_refit.split('.')[0] + '_al.nii.gz'

    # directly apply the transforms to the refitted t1 and extra images, to QC waypoint registration
    if xfm_qc is True:
        print('# Quality-checking waypoint registration for {}...'.format(vol_refit))
        print('  Check {} in AFNI, with the refitted preop CT as reference...'.format(vol_xfm_al_fn))

        with open(os.devnull, 'w') as fnull:
            call(['3dAllineate',
                '-1Dmatrix_apply',os.path.join(ppr_dir, vol_xfm_fn),
                '-prefix', vol_xfm_al_fn,
                vol_refit],
                stdout=fnull, stderr=fnull)

    return vol_xfm

def combine_xfm(mcp_xfm, vol_xfm, vol_id, ppr_dir, vol_refit, preop_ct):

    if preop_ct is True:
        final_xfm = mcp_xfm
    else:
        final_xfm = np.dot(vol_xfm, mcp_xfm)

    final_xfm_out = '{} {} {} {}'.format(str(final_xfm[0, :])[1:-1], str(final_xfm[1, :])[1:-1],
                        str(final_xfm[2, :])[1:-1], str(final_xfm[3, :])[1:-1])

    # let's make a filename using the ID, and save it
    final_xfm_out_fn = vol_id + '_final_transform.1D'
    prettysave(final_xfm_out, final_xfm_out_fn, ppr_dir)

    print('# Applying ACPC/MCP transform to {}...\n'.format(vol_refit))

    with open(os.devnull, 'w') as fnull:
        call(['3dAllineate',
            '-1Dmatrix_apply', os.path.join(ppr_dir, final_xfm_out_fn),
            '-prefix', vol_refit.split('.')[0] + '_mcp.nii.gz',
            vol_refit],
            stdout=fnull, stderr=fnull)

# saves the output to the current directory
def prettysave(item, fn, ppr_dir):
    dir_path = os.path.join(ppr_dir,fn)
    if not os.path.exists(dir_path):
        with open(dir_path, 'w') as out:
            out.write("{}\n".format(item))
    else:
        print(fn)
        print('# File already exists, will not overwrite.\n')

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script pre-processes MR and CT images, \
        bringing them all to the same waypoint ACPC/MCP coordinate space')

    parser.add_argument('-ppr_dir', action='store',
        help='the location of ppr directory generated by read_ppr.py', required=True)

    parser.add_argument('-preop_ct', action='store',
        help='the volume used for preop ct data (assumed to be I1) - required', required=True)

    parser.add_argument('-preop_t1', action='store',
        help='the volume used for preop t1 data (assumed to be I2) - required', required=True)

    parser.add_argument('-extra1', action='store',
        help='the volume used for other imaging data (post- or intra-op) (assumed to be I3)')

    parser.add_argument('-extra2', action='store',
        help='the volume used for other imaging data (post- or intra-op) (assumed to be I4)')

    parser.add_argument('-intraop_ct', action='store',
        help='label of input dataset that was acquired from the intra-op CT (extra1,extra2) \
            - flags for special image orientation refitting')

    parser.add_argument('-xfm_qc', action='store_true', default=False,
        help='apply initial waypoint registrations (no ACPC alignment) to refitted images \
            to make sure registrations are accurate')

    parser.add_argument('-save_refit', action='store_true', default=False,
        help='flag to save intermediate refitted volumes')

    parser.add_argument('-image_order', action='store',
        help='list of waypoint (I)mage values associated with registration order - \
            you likely will not need this option', nargs='*')

    args = parser.parse_args()

    waypoint_convert(args.ppr_dir, args.preop_ct, args.preop_t1, args.extra1, args.extra2,
        args.intraop_ct, args.xfm_qc, args.save_refit, args.image_order)

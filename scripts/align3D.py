#!/usr/bin/env python

# last change (2017-11-05): improved readability

# align3D.py - adapted from Dr. Andrei Barborica's align3D.m.
# Returns a transformation matrix to align two vectors in 3D space

# vec1 = can be a single a column vector containing 3D cartesian coordinates
#     of the vector that is being aligned [x; y; z] or it can be a
#     two-column array containing the start point and end point of the vector,
#     in the form  [xstart xend; ystart yend; zstart zend];
# vec2 = a one-column vector or two-column array containing the coordinates of the
#           destination vector;

import numpy as np
import axel_rot

def align3D(vec1, vec2):

    # Make sure input vectors are in column form - if not, transpose
    if vec1.shape[0] < vec1.shape[1]:
        vec1=vec1.transpose()
        vec2=vec2.transpose()

    # If vectors are in [start end] form, translate first vector to the
    # the start point of the second one
    if vec1.shape[1] > 1:
        vec1_d = vec1[:, 1] - vec1[:, 0]
        vec2_d = vec2[:, 1] - vec2[:, 0]
        x_vec = np.cross(vec1_d, vec2_d)
        theta = np.arccos(np.dot(vec1_d, vec2_d) / \
            (np.linalg.norm(vec1_d) * np.linalg.norm(vec2_d)))
    else:
        x_vec = np.cross(vec1[:, 0], vec2[:, 0])
        theta = np.arccos(np.dot(vec1[:, 0], vec2[:, 0]) / \
            ((np.linalg.norm(vec1[:, 0]) * np.linalg.norm(vec2[:, 0]))))

    # changed rotation input from degrees, keeping as radians
    M = axel_rot.axel_rot(theta, x_vec, np.array([0, 0, 0]))
    nn = M.shape[0]
    rot = M[0:(nn-1), 0:(nn-1)]
    #shift = M[:,nn-1][0:nn-1]
    shift = vec2[:, 0] - np.dot(rot, vec1[:, 0])

    mat_fin = axel_rot.mkaff(rot, shift)

    return mat_fin

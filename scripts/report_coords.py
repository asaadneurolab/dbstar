#!/usr/bin/env python

# last change (2017-11-28): added terminal output when '-dbs' option is used

# report_coords.py - converts the output coordinates of calc_depth_coords.py,
# test_fs_location.py, and warp_depth_coords.py into a standardized
# spreadsheet format

# if your default python is python3.* -> install the pyexcel-ods3 package
# (https://github.com/pyexcel/pyexcel-ods3)
# if your default python is python2.7.* -> install the pyexcel-ods package
# (https://github.com/pyexcel/pyexcel-ods)

# KNOWN BUG: for the final anat_test column, if a result is negative
# (i.e. recording or final coordinate is not within the ROI), pyexcel-ods
# (python2.7.*) will return blank values, whereas pyexcel3 (python3.*)
# will return 0 values. They should both return 0s.

import argparse as ap
import numpy as np
import os
import sys
from collections import OrderedDict

if sys.version_info[0] == 2:
    from pyexcel_ods import save_data
elif sys.version_info[0] == 3:
    from pyexcel_ods3 import save_data
else:
    print('ERROR: Unrecognized python version...cannot load pyexcel_ods*')
    sys.exit()

def report_coords(coord_dir, fs_dir, atlas_dir, trajs, depths, session_id,
    subj_id, target, ref_image, track, macro, final_traj, final_depth,
    anat_test, com, ttn27_mc, mni_mc, t1_warp, dbs):

    print('\n---report_coords.py---\n')

    # check directory inputs
    if coord_dir is None:
        coord_dir = 'coords'

    if fs_dir is None:
        fs_dir = 'fs_test'

    if atlas_dir is None:
        atlas_dir = 'tt_warp'

    # check if there is an input track surface for a track ID
    if track is None:
        print('ERROR: No input surface! (-track)')
        sys.exit()

    # create an ID for output coordinates
    track_id = track.split('.')[0]

    # check trajectory/final trajectory/depth/session ID input
    if trajs is None:
        print('ERROR: No recording trajectories specified! (-trajs)')
        sys.exit()
    else:
        calc_traj = verbatim_lines(trajs, 'parse')
        
    if final_traj is None:
        print('# Warning: no final trajectory (-final_traj) specified -')
        print('  is this an intra-op image?')
    else:
        target_traj = verbatim_lines(final_traj, 'parse')

    # check if dbs contact coordinates are specified
    # all depths are calculated b/w center of contact spans
    if dbs is not None:
        depths = 'dbs'
        print('# Reporting DBS contact coordinates for {} electrodes...\n'.format(dbs))

    if final_depth is None:
        print('# Warning: no final electrode depth (-final_depth) -')
        print('  is this an intra-op image?')
    else:
        final_d2t = np.loadtxt(final_depth)

    if depths is None:
        print('ERROR: No input depth values! (-depths)')
        sys.exit()
    elif depths == 'dbs':
        if dbs == '3389':
            d2t = np.array([0.75, 2.75, 4.75, 6.75]) + final_d2t
        elif dbs == '3387':
            d2t = np.array([0.75, 3.75, 6.75, 9.75]) + final_d2t
    else:
        d2t = np.append(np.loadtxt(depths))

    d2t = np.append(d2t, 0.0)

    if session_id is None:
        print('ERROR: No task session IDs specified! (-session_id)')
        sys.exit()
    elif depths == 'dbs':
        task_id = ['c0', 'c1', 'c2', 'c3']
        task_id = [con + '_{}'.format(dbs) for con in task_id]
    else:
        task_id = verbatim_lines(session_id, 'parse')

    # check that depth and session_id values are the same length
    # (i.e. each task/recording session depth has a matching ID)
    if np.shape(d2t)[0]-1 is not np.shape(task_id)[0]:
        print('ERROR: Session IDs (-session_id) not the same length as')
        print('       depth values (-depths)')
        sys.exit()

    # obtain patient ID from current directory if not specified
    if subj_id is None:
        subj_id = os.path.basename(os.getcwd())

    # check that a target was specified
    if target is None:
        print('ERROR: No surgical target (-target) specified!')
        sys.exit()

    # check that a reference image description was specified
    if ref_image is None:
        print('# Warning: No reference image description (-ref_image) specified!')
        
    # modify the reference image info if intra-patient T1 warps
    # were used (-t1_warp)
    if t1_warp is True:
        print('# Adding "_t1warp" to image notes...\n')
        ref_image = ref_image + '_t1warp'

    # check if we also reporting macro electrode coordinates?
    if macro is True:
        print('# Reporting macro-electrode depths...\n')
    elif dbs is False:
        print('# Reporting micro-electrode depths...\n')

    # check if we are testing for a specific anatomical target
    if anat_test is not None:
        print('# Checking to see if coordinates are in {}...\n'.format(anat_test))

    # check if we are loading any center-of-mass coordinates
    if com is True:
        print('# Loading center-of-mass coordinates...make sure -coord_dir')
        print('  points to "com/"!\n')

    # check if we calculating atlas y-coordinates relative to MC
    # (TTN27-atlas specific)
    if ttn27_mc is True:
        print('# Reporting atlas coordinates relative to MC, not AC...\n')
        atlas_y = 'atlas_y_posterior+_MC0'
    else:
        atlas_y = 'atlas_y_posterior+_AC0'

    # initialize some spreadsheet parameters
    go_macro = False
    
    if com is True:
        out_sheet_fn = subj_id + '_' + target + '_com.ods'
    elif t1_warp is True:
        out_sheet_fn = subj_id + '_' + target + '_t1warp.ods'
    else:
        out_sheet_fn = subj_id + '_' + target + '.ods'
        
    out_sheet = OrderedDict()

    if com is True:
        header = ['ID','Target','Session','Trajectory','Depth_above+',
            'native_x_left+','native_y_posterior+_MC0','native_z_superior+',
            'native_euclid','image_notes','FS_atlas_region','atlas_warp_RMS',
            'atlas_x_left+',atlas_y,'atlas_z_superior+','atlas_euclid','TT_atlas_1mm']
    else:
        header = ['ID','Target','Session','Trajectory','Depth_above+',
            'native_x_left+','native_y_posterior+_MC0','native_z_superior+',
            'image_notes','FS_atlas_region','atlas_warp_RMS','atlas_x_left+',
            atlas_y,'atlas_z_superior+','TT_atlas_1mm']

    # add an anatomy test field to the header if indicated
    if anat_test is not None:
        # for GP testing, list two columns, one for GPi, the other for GPe
        if anat_test == 'GP':
            header.append('GPi_anat_test_present1')
            header.append('GPe_anat_test_present1')
        else:
        # every other case
            header.append(anat_test + '_anat_test_present1')
    
    # add the header to the spreadsheet
    out_sheet.update({'Sheet 1': [header]})

    # add the macro-electrode data to the spreadsheet
    # (if user indicated by '-macro')
    if macro is True:
        go_macro = True        
        out_sheet = sheet_writer(coord_dir, fs_dir, atlas_dir, calc_traj,
            d2t, task_id, subj_id, target, ref_image, target_traj, final_d2t,
            go_macro, track_id, out_sheet, anat_test, com, ttn27_mc, mni_mc,
            t1_warp)
    else:
        # add the micro-electrode data to the spreadsheet
        out_sheet = sheet_writer(coord_dir, fs_dir, atlas_dir, calc_traj, d2t,
            task_id, subj_id, target, ref_image, target_traj, final_d2t, go_macro,
            track_id, out_sheet, anat_test, com, ttn27_mc, mni_mc, t1_warp)

    # save the spreadsheet
    if os.path.isfile(out_sheet_fn):
        print(out_sheet_fn)
        print('# File already exists, will not overwrite.')
    else:
        save_data(out_sheet_fn, out_sheet)

def sheet_writer(coord_dir, fs_dir, atlas_dir, calc_traj, d2t, task_id,
    subj_id, target, ref_image, target_traj, final_d2t, go_macro, track_id,
    out_sheet, anat_test, com, ttn27_mc, mni_mc, t1_warp):

    # for each depth value...
    for d, depth in enumerate(d2t):
        # for each electrode trajectory at each depth value...
        for t in calc_traj:
            # use the calc_depth_coords.py filename structure to locate
            # the depth-trajectory coordinate file
            if go_macro is True:
                fn_prefix = track_id + '_macro_' + str(depth) + '_' \
                    + t.lower() + '_track'
            else:
                fn_prefix = track_id + '_' + str(depth) + '_' \
                    + t.lower() + '_track'

            if com is True:
                coord_prefix = fn_prefix + '.com.coord'
            elif t1_warp is True:
                coord_prefix = fn_prefix + '.coord.t1warp.coord'
            else:
                coord_prefix = fn_prefix + '.coord'

            coord_fn = os.path.join(coord_dir, coord_prefix)

            # only proceed if the depth-trajectory coordinate file is present
            if os.path.isfile(coord_fn) is True:

                # based on the depth-trajectory coordinate filename,
                # calculate the freesurfer- and atlas-related data filenames
                # load the coordinate in native space
                temp_coord = np.loadtxt(coord_fn)
                # load the native freesurfer segmentation depth-trajectory
                # coordinate test info
                fs_prefix = fn_prefix + '.coord.fs.test'
                fs_fn = os.path.join(fs_dir, fs_prefix)
                # check if freesurfer-file exists
                # (freesurfer doesn't always finish)
                if os.path.isfile(fs_fn):
                    fs_test = open(fs_fn)
                    fs_test = fs_test.readline()
                else:
                    fs_test = 'file not found'

                # load the appropriate atlas-warped depth-trajectory coordinate
                if com is True:
                    atlas_prefix = fn_prefix + '.iwarp.com.coord'
                    atlas_fn = os.path.join(coord_dir, atlas_prefix)
                elif t1_warp is True:
                    atlas_prefix = fn_prefix + '.coord.t1warp.iwarp.coord'
                    atlas_fn = os.path.join(atlas_dir, atlas_prefix)
                else:
                    atlas_prefix = fn_prefix + '.iwarp.coord'
                    atlas_fn = os.path.join(atlas_dir, atlas_prefix)
                
                temp_atlas_coord = np.loadtxt(atlas_fn)
                
                # if -ttn27_mc is called, subtract 11.5mm from atlas_y
                # (calculated MC between AC origin [0,0,0] and PC [0,23,0]
                # same for mni
                if ttn27_mc is True:
                    temp_atlas_coord[1] = temp_atlas_coord[1] - 11.5
                if mni_mc is True:
                    temp_atlas_coord[1] = temp_atlas_coord[1] - 11
                    temp_atlas_coord[2] = temp_atlas_coord[2] - 2
                
                # If center-of-mass coordinates are indicated, set prefix
                # back to default to load other atlas-specific data
                if com is True:
                    atlas_prefix = fn_prefix + '.iwarp.coord'

                # load the corresponding text 'whereami' output for each
                # atlas coordinate test
                atlas_test_prefix = atlas_prefix + '.atlas.test'
                atlas_test_fn = os.path.join(atlas_dir, atlas_test_prefix)
                atlas_test = verbatim_lines(atlas_test_fn, '')

                # load the RMS value from the non-linear warping from patient->atlas
                rms_fn = os.path.join(atlas_dir, 'anat.un.aff.nii_WarpDrive.log')
                temp_rms = verbatim_lines(rms_fn, 'parse')
                rms = temp_rms[2]

                # parse the AFNI 'whereami' output for anatomical regions where
                # the coordinate is located (currently set at 1mm radius range)
                for l, line in enumerate(atlas_test):
                    if '  Focus point' in atlas_test[l].split(':')[0]:
                        TT_atlas_1mm = atlas_test[l].split(':')[1]
                        TT_atlas_1mm = parse_whereami(TT_atlas_1mm, atlas_test, l)

                    if 'Within 1 mm' in atlas_test[l].split(':')[0]:
                        TT_atlas_1mm = TT_atlas_1mm + ',' + atlas_test[l].split(':')[1]
                        TT_atlas_1mm = parse_whereami(TT_atlas_1mm, atlas_test, l)

                # based on the parsed AFNI 'whereami' output, is our region of
                # interest present within 1mm of the recording site? (1=yes, 0=no)
                if anat_test is not None:
                    anat_test_result = check_anat(TT_atlas_1mm, anat_test)

                # now we build each spreadsheet row in accordance with the header
                out_row = [subj_id]

                # micro or macro label
                if go_macro is True:
                    target_out = target + '_macro'
                else:
                    target_out = target
                # regardless, add a center-of-mass label if indicated
                if com is True:
                    target_out = target_out + '_com'
                    
                out_row.append(target_out)

                # if this is a final implant coordinate, note the
                # final trajectory chosen
                if d > np.shape(task_id)[0]-1:
                    out_row.extend(['Target (' + target_traj[0].upper() + ')',
                        t.upper(),float(final_d2t)])
                else:
                    out_row.extend([task_id[d].upper(), t.upper(), float(depth)])
                
                # build the rest of the spreadsheet row
                if com is True:
                    out_row.extend([float(temp_coord[0]), float(temp_coord[1]),
                        float(temp_coord[2]), float(temp_coord[3]), ref_image,
                        fs_test, float(rms), float(temp_atlas_coord[0]),
                        float(temp_atlas_coord[1]), float(temp_atlas_coord[2]),
                        float(temp_atlas_coord[3]), TT_atlas_1mm])
                else:
                    out_row.extend([float(temp_coord[0]), float(temp_coord[1]),
                        float(temp_coord[2]), ref_image, fs_test, float(rms),
                        float(temp_atlas_coord[0]), float(temp_atlas_coord[1]),
                        float(temp_atlas_coord[2]), TT_atlas_1mm])
                
                # add the atlas anatomy test result (e.g. is this coordinate in STN?)
                if anat_test is not None:
                    if np.shape(anat_test_result)[0] > 1:
                        # multiple ROI tests (e.g. '-anat_test GP' tests both GPi
                        # and GPe, reports in separate columns)
                        for anat_result in anat_test_result:
                            out_row.append(float(anat_result))
                    else:
                        # single ROI test
                        out_row.append(float(anat_test_result[0]))

                # extract the output sheet data, add our new row to it,
                # create a new output sheet to return
                new_rows = out_sheet.get('Sheet 1')
                new_rows.extend([out_row])
                out_sheet.update({'Sheet 1': new_rows})

            else:
                print('# Warning - {} not found...skipping.'.format(coord_prefix))

    return out_sheet

def parse_whereami(atlas_out, atlas_test, l):
    # bare-bones utility for parsing out specific anatomical
    # regions indicated in AFNI's 'whereami' output
    no_more_and = False
    next = 1
    while no_more_and is False:
        if '-AND-' in atlas_test[l + next]:
            atlas_out = atlas_out + ',' + atlas_test[l+next].split('-AND-')[1]
            next += 1
        else:
            no_more_and = True

    return atlas_out

def check_anat(atlas_out, anat_test):
    test_str = 'None'    
    anat_test_result = []

    # dictionary of possible regions-of-interest to test...
    # can easily expand/be made into a larger dictionary
    if 'VL' in anat_test:
        test_str = ['Ventral Lateral Nucleus']
    if 'VIM_mni' in anat_test:
        test_str = ['Ventro_intermedius_internus(V.im.i)']
    if 'STN' in anat_test:
        test_str = ['Subthalamic Nucleus']
    if 'STN_mni' in anat_test:
        test_str = ['Subthalamic_nucleus(Sth)']
    if 'GPi' in anat_test:
        test_str = ['Medial Globus Pallidus']
    if 'GPe' in anat_test:
        test_str = ['Lateral Globus Pallidus']
    if anat_test == 'GP':
        test_str = ['Medial Globus Pallidus', 'Lateral Globus Pallidus']

    if test_str is not 'None':
        for t_str in test_str:
            if t_str in atlas_out:
                anat_test_result.append(1)
            else:
                anat_test_result.append(0)
    else:
        anat_test_result.append(0)
        
    return anat_test_result

def verbatim_lines(f, parse):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters
        # (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]

        if 'parse' in parse:
            traj = str(lines[0])
            verbatim_traj = traj.split()
            return verbatim_traj    
        else:
            return lines

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='# This script converts the output \
        coordinates (one track at a time) of calc_depth_coords.py, test_fs_location.py, \
        and warp_depth_coords.py into a standardized spreadsheet format - you \
        need to install the pyexcel-ods3 (python3.*) or pyexcel-ods (python2.7.*) \
        package (https://github.com/pyexcel/pyexcel-ods3 ')

    parser.add_argument('-coord_dir', action='store',
        help='directory containing coordinates to test (typically generated by \
            calc_depth_coords.py) (default: coords)')

    parser.add_argument('-fs_dir', action='store',
        help='directory containing results of native anatomy tests generated by \
            test_fs_location.py (default:  fs_test)')

    parser.add_argument('-atlas_dir', action='store',
        help='output directory generated by tt_align.py containing non-linear \
            warp volumes and transforms - also the output directory for warped \
            coordinates (default: tt_warp)')

    parser.add_argument('-trajs', action='store',
        help='text file containing which trajectory coordinates (e.g. A, C, P) \
            were calculated')

    parser.add_argument('-depths', action='store',
        help='text file containing trajectory depths were calculated into coordinates')

    parser.add_argument('-session_id', action='store',
        help='text file containing task IDs corresponding with trajectory \
            depths (e.g. "A B C")')

    parser.add_argument('-subj_id', action='store',
        help='anonymized ID of patient (default: patient directory name)')

    parser.add_argument('-target', action='store',
        help='the surgical target of the track (e.g. "L VIM")')

    parser.add_argument('-ref_image', action='store',
        help='the type of intra- or post-op image used for coordinate \
            calculation (e.g. "postop_CT_thin_slice")')

    parser.add_argument('-track', action='store',
        help='the clipped gifti surface (*clp.gii) of an electrode track')

    parser.add_argument('-macro', action='store_true', default=False,
        help='flag that also calculates/outputs the location of the macro-electrode \
            for each micro-electrode recording site')

    parser.add_argument('-final_traj', action='store',
        help='text file containing which trajectory/trajectories (e.g. A, C, P) the \
            -track gifti surface represents')

    parser.add_argument('-final_depth', action='store',
        help='text file containing the trajectory depth where the electrode was \
            implanted (postop images only)')

    parser.add_argument('-anat_test', action='store',
        help='specify a DBS target to test (VL, STN, GP(e/i))')

    parser.add_argument('-com', action='store_true', default=False,
        help='flag that loads coordinates calculated relative to a target volume center-of-mass')

    parser.add_argument('-ttn27_mc', action='store_true', default=False,
        help='flag that reports TTN27-atlas coordinates relative to MC, not AC (subtracts 11.5 mm from y')

    parser.add_argument('-mni_mc', action='store_true', default=False,
        help='flag that reports MNI-atlas coordinates relative to MC, not AC (subtracts 11 mm from y, 2 mm from z')
    
    parser.add_argument('-t1_warp', action='store_true', default=False,
        help='flag that reports that intra-patient T1 warps were used')

    parser.add_argument('-dbs', action ='store',
        help='flag to pre-load medtronic DBS contact coordinates (options: 3387, 3389)')

    args = parser.parse_args()

    report_coords(args.coord_dir, args.fs_dir, args.atlas_dir, args.trajs,
        args.depths, args.session_id, args.subj_id, args.target,
        args.ref_image, args.track, args.macro, args.final_traj,
        args.final_depth, args.anat_test, args.com, args.ttn27_mc,
        args.mni_mc, args.t1_warp, args.dbs)

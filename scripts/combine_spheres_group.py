#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes

# combine_spheres_group.py - generates a directory and AFNI volumes for visualizing atlas-warped
# recording and final electrode placement coordinates across subjects

import argparse as ap
import numpy as np
import os
from subprocess import call
import sys

def combine_spheres(record_fig, final_fig, phys_fig, atlas_vol, atlas_dir, out_dir, anat_label):

    print('\n---combine_spheres_group.py---\n')

    # define the warp-target-atlas volume
    if atlas_vol is None:
        atlas_vol = '/usr/local/abin/TT_N27+tlrc'

    # define the patient atlas-warp directory
    if atlas_dir is None:
        atlas_dir = 'tt_warp/'

    # define the output directory
    if out_dir is None:
        out_dir = 'atlas_figure'

    if anat_label is None:
        print('ERROR: No figure label specified (-anat_label)!')
        sys.exit()

    # create the output directory if it doesn't exist
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
        # add the atlas volume to the output directory
        call(['3dcopy', atlas_vol, out_dir + '/'])

    # need to specify a figure to create
    if record_fig is None and final_fig is None and phys_fig is None:
        print('# No figure specified, nothing for combine_spheres_group.py to do.')
        sys.exit()

    # create a recording location figure
    if record_fig is not None:
        # check if recording location figure instructions exist
        if not os.path.isfile(record_fig):
            print('ERROR: -record_fig file does not exist!')
            sys.exit()
        else:
            fig_type = 'record_loc'
            r_fig = verbatim_lines_traj(record_fig)
            make_fig(r_fig, atlas_dir, out_dir, fig_type, anat_label)
        
    # create a recording location figure
    if final_fig is not None:
        # check if final location figure instructions exist
        if not os.path.isfile(final_fig):
            print('ERROR: -final_fig file does not exist!')
            sys.exit()
        else:
            fig_type = 'final_loc'
            f_fig = verbatim_lines_traj(final_fig)
            make_fig(f_fig, atlas_dir, out_dir, fig_type, anat_label)

    # create a physiology data figure
    if phys_fig is not None:
        # check if phys location figure instructions exist
        if not os.path.isfile(phys_fig):
            print('ERROR: -phys_fig file does not exist!')
            sys.exit()
        else:
            print('# Phys fig: no within-subject normalization performed!\n')
            fig_type = 'phys_loc'
            f_fig = verbatim_lines_traj(phys_fig)
            make_fig(f_fig, atlas_dir, out_dir, fig_type, anat_label)
            

def make_fig(fig, atlas_dir, out_dir, fig_type, anat_label):

    temp_sphere_count = 0
    total_sphere_count = 0
    sphere_list = []
    multi_spheres = 1
    output_list = []

    for f in fig:
        # for each figure instruction line, split the line into [sj track]
        f_split = f.split()
        sj_dir = f_split[0]
        sj_sphere = f_split[1]

        sphere_list.append(os.path.join(sj_dir, atlas_dir, sj_sphere))
        temp_sphere_count += 1
        
        if fig_type == 'phys_loc':
            total_sphere_count += 0
        else:
            total_count_temp = np.loadtxt(os.path.join(sj_dir, atlas_dir, sj_sphere.split('.')[0] + '_count'))
            total_sphere_count += total_count_temp

        if temp_sphere_count == 26:
            if fig_type == 'phys_loc':
                total_sphere_count += 26

            output_list.append(common_sphere(sphere_list, multi_spheres, out_dir, fig_type,
                '', total_sphere_count, anat_label))
            multi_spheres += 1
                       
            temp_sphere_count = 0
            sphere_list = []   

    if sphere_list != []:
        if fig_type == 'phys_loc':
            total_sphere_count += temp_sphere_count
        
        output_list.append(common_sphere(sphere_list, multi_spheres, out_dir, fig_type,
            '', total_sphere_count, anat_label))

    # normalization stage
    common_sphere(output_list, multi_spheres, out_dir, fig_type, 'normalize', total_sphere_count, anat_label)

def common_sphere(sphere_list, multi_spheres, out_dir, fig_type, norm, total_sphere_count, anat_label):
        abc = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
        comm = '3dcalc'
        expr = ' -expr "('

        if 'normalize' in norm:
            output_fn = out_dir + '/' + anat_label + '_' + fig_type + '_sphere_normalized.nii.gz'
        else:
            output_fn = out_dir + '/' + anat_label + '_' + fig_type + '_sphere' + str(multi_spheres) + '.nii.gz'

        prefix = '" -prefix ' + output_fn

        for s, fl in enumerate(sphere_list):
            comm = comm + ' -' + abc[s] + ' ' + fl
            if s is 0:
                expr = expr + abc[s]
            else:
                expr = expr + '+' + abc[s]

        if 'normalize' in norm:
            comm = comm + expr + ')/' + str(total_sphere_count) + prefix
        else:
            comm = comm + expr + ')' + prefix

        # I would like to suppress output, but the non-overwrite warnings are too important to
        # miss (to suppress completely, add 'stderr=fnull' to call
        with open(os.devnull, 'w') as fnull:    
            call(str(comm), shell=True)

        return output_fn


def verbatim_lines_traj(f):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]
        #traj = str(lines[0])
        #verbatim_traj = traj.split()

    #return verbatim_traj
        return lines

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='# This script consolidates the output spheres from \
        make_spheres.py into one directory')

    parser.add_argument('-record_fig', action='store',
        help='two-column text file containing subj_id and recording track most appropriate for figure')

    parser.add_argument('-final_fig', action='store',
        help='two-column text file containing subj_id and final location track most appropriate for figure')

    parser.add_argument('-phys_fig', action='store',
        help='two-column text file containing subj_id and physiology track most appropriate for figure')

    parser.add_argument('-atlas_vol', action='store',
        help='a filepath leading to the same atlas at another location/another atlas to use for \
            non-linear registration (default: /usr/local/abin/TT_N27+tlrc)')

    parser.add_argument('-atlas_dir', action='store',
        help='output directory generated by atlas_align.py containing non-linear warp volumes and \
            transforms - also the output directory for warped coordinates (default: tt_warp)')

    parser.add_argument('-out_dir', action='store',
        help='specify the output directory for the figures (default: atlas_figure)')

    parser.add_argument('-anat_label', action='store',
        help='describe the anatomical area for figure output')

    args = parser.parse_args()

    combine_spheres(args.record_fig, args.final_fig, args.phys_fig, args.atlas_vol, args.atlas_dir,
        args.out_dir, args.anat_label)

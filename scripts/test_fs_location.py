#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes,
#
#                           now using aparc.a2009s+aseg_REN_gm.nii.gz as default seg_vol

# test_fs_location.py - determines where depth coordinates are located within
# a patient's segmented T1 volume, using AFNI's '3dmaskdump' function

import argparse as ap
from subprocess import call
import numpy as np
import os
import sys

def test_fs_location(coord_dir, t1_vol, seg_vol, out_dir, make_spheres, no_convert):

    print('\n---test_fs_location.py---\n')

    # check inputs
    if coord_dir is None:
        coord_dir = 'coords'

    if t1_vol is None:
        print('ERROR: No input T1 volume! (-t1_vol)')
        sys.exit()

    # define the segmentation volume
    if seg_vol is None:
        seg_vol = './aparc.a2009s+aseg_REN_gm.nii.gz'

    # make the output directory if it doesn't exist
    if out_dir is None:
        out_dir = 'fs_test'

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # make a copy of the t1 volume for sphere-checking
    if make_spheres is True:
        print('# Generating spheres for native coordinate visualization...')
        call(['3dcopy', t1_vol, os.path.join(out_dir, t1_vol)])

    if no_convert is True:
        print('# Saving raw 3dmaskdump output...')

    print('# Testing coordinates in freesurfer segmentation space...')
    # let's test each *.coord in the coordinate directory,
    # and generate a 1mm radius sphere at each coordinate
    for file in os.listdir(coord_dir):
        if file.endswith('.coord'):
            coord_fn = os.path.join(coord_dir, file)

            # check if the coordinate file is empty, if not load it
            if os.stat(coord_fn).st_size == 0:
                print('# {} is empty...skipping'.format(file))
            else:
                coord=np.loadtxt(coord_fn)

                # freesurfer test w/ 1mm radius
                fs_test_fn = os.path.join(out_dir, (file + '.fs.test'))
                if os.path.isfile(fs_test_fn) is True:
                    print(fs_test_fn)
                    print('# File already exists, will not overwrite.')
                else:
                    with open(fs_test_fn, 'w') as f:
                        call(['3dmaskdump',
                            '-quiet',
                            '-dball', str(coord[0]),
                            str(coord[1]), str(coord[2]),
                            '1', seg_vol],
                            stdout=f)

                # convert the standard 3dmaskdump output to a
                # human-readable format (unless specified to skip)
                if no_convert is False:
                    fs_test_convert(fs_test_fn)
                
                # 1mm (srad) sphere generation
                if make_spheres is True:
                    # make a sphere for each native coordinate
                    call(['3dUndump',
                        '-master', t1_vol,
                        '-xyz',
                        '-dval', '1',
                        '-srad', '1',
                        '-orient', 'RAI',
                        '-prefix', os.path.join(out_dir,  (file + '.sphere.nii')),
                        coord_fn])

# converting AFNI voxel values to a freesurfer segmentation label (currently support thalamus (for VL/ViM), ventralDC (for STN), and pallidum (for GP(e/i)))
def fs_test_convert(fn):
    
    fs_test_output = np.loadtxt(fn)
    fs_label = fs_test_output[:, 3]

    with open(fn, 'w') as f:
        if 7 in fs_label:
            f.write('L_thal_proper')
        elif 27 in fs_label:
            f.write('R_thal_proper')
        elif 18 in fs_label:
            f.write('L_ventralDC')
        elif 34 in fs_label:
            f.write('R_ventralDC')
        elif 10 in fs_label:
            f.write('L_pallidum')
        elif 30 in fs_label:
            f.write('R_pallidum')
        else:
            f.write('unknown location')

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script determines where depth \
        coordinates are located within a patient-specific anatomical segmentation, \
        outputting to plaintext files')

    parser.add_argument('-coord_dir', action='store',
        help='directory containing coordinates to test (typically generated by \
            calc_depth_coords.py) (default: coords)')

    parser.add_argument('-t1_vol', action='store',
        help='a T1 nifti volume brought to the ACPC/MCP space by \
            waypoint_convert.py (e.g. t1_pre_refit_mcp.nii.gz)')

    parser.add_argument('-seg_vol', action='store',
        help='the freesurfer-segmented volume (typically generated by \
            t1_acpc_seg.py) to test coordinates against (default: \
            ./aparc.a2009s+aseg_REN_gm.nii.gz)')

    parser.add_argument('-out_dir', action='store',
        help='specify the output directory for coordinate test information \
            (default: fs_test)')

    parser.add_argument('-make_spheres', action='store_true', default=False,
        help='flag to generate native coordinate 1mm-radius spheres for each \
            recording location - warning: may take up a lot of disk space')

    parser.add_argument('-no_convert', action='store_true', default=False,
        help='flag to skip conversion of freesurfer test output into a \
            corresponding anatomical label (e.g. voxel value of 6 = L_thal_proper)')

    args = parser.parse_args()

    test_fs_location(args.coord_dir, args.t1_vol, args.seg_vol, args.out_dir,
        args.make_spheres, args.no_convert)

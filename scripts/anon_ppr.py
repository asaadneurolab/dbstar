#!/usr/bin/env python

# last change (2017-11-05): minor readability changes

# anon_ppr.py - anonymize PHI from PPR files (*.ppr) exported from Waypoint Planner

import argparse as ap
import os
import re
import sys

def anon_ppr(fn):
    print('\n---anon_ppr.py---\n')

    print('# Anonymizing ' + fn + '...\n')

    lines_all = verbatim_lines(fn)

    # for later
    lines_anon = lines_all

    # zero indexed!
    # keys are unordered
    # to order keys, make a list of the keys and change the order
    # as necessary
    # keys are unique, values are not necessarily
    # spaces are sacred
    lineidx = {
        '// DICOM tag of Patient ID': 9,
        'Patient     ID': 12,
        'I1  Study  UID': 13,
        'I1  Series UID': 14,
        'I2  Study  UID': 17,
        'I2  Series UID': 18,
        'I3  Study  UID': 21,
        'I3  Series UID': 22,
        'I4  Study  UID': 25,
        'I4  Series UID': 26,
    }

    # create a dictionary from the line index dictionaries
    phi = dict.fromkeys(lineidx)

    # iterate through our phi keys, placing anonymized lines in lines_anon
    for key in lineidx:
        lines_anon = parse_anon_val(lines_all, lineidx, key, lines_anon)

    #anonymize filename
    anon_fn = anon_ppr_fn(fn)

    #write anonymized lines to disk
    print('# Saving as ' + anon_fn + '...')
    prettysave(lines_anon, anon_fn)

def parse_anon_val(lines_all, idx_dict, key, lines_anon):
    # locate a line by the phi key
    i = idx_dict[key]
    str_line = lines_all[i]

    if i == 9:
        anon_items = []
        line_parts = str_line.split(", ")
        for j in line_parts:
            # replace digits (/d) with 0s to anonymize data
            anon_items.append(re.sub('\d', '0', j))

        # anonymize final character of DICOM UIDs as 'A', always
        for j, elem in enumerate(anon_items):
            if elem[-1].isalpha():
                mod_elem = list(elem)
                mod_elem[-1] = 'A'
                anon_items[j] = ''.join(mod_elem)

        # join it all together
        anon_line = ', '.join(anon_items)
        lines_anon[i] = anon_line

    else:
        # split a line by the ": " character
        line_parts = str_line.split(": ")
        key_test = line_parts[0]

        # if the PPR text line is what you say it should be...
        if key_test == key:
            val = line_parts[1]
            # replace digits (/d) with 0s to anonymize data
            anon_val=re.sub('\d', '0', val)

            # add the modified/anonymized line back to lines_anon
            anon_line = key_test + ': ' + anon_val
            lines_anon[i] = anon_line

        else:
            print(key + ' not found')

    return lines_anon

def verbatim_lines(f):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]

    return lines

def prettyprint(item_iterable):
    for item in item_iterable:
        print(item)

# saves the output
def prettysave(item_iterable, fn):
    if not os.path.exists(fn):
        with open(fn, 'w') as out:
            for item in item_iterable:
                out.write("{}\n".format(item))
    else:
        print('# File already exists, will not overwrite.')

def anon_ppr_fn(fn):
    fn_parts = fn.split('.')
    ID_anon = fn_parts[0] + '_anon'
    anon_fn = ID_anon + '.' + fn_parts[1]

    return anon_fn

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='anonymize PHI from PPR files (*.ppr) \
        exported from Waypoint Planner')
    parser.add_argument('filename')
    args = parser.parse_args()

    anon_ppr(args.filename)

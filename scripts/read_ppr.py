#!/usr/bin/env python

# last change (2017-11-05): improved readability

# read_ppr.py - parses Waypoint planner data for AFNI translation

# BUG - r_p_screw_base does not always successfully export...I suspect that it's a logic problem

import argparse as ap
import numpy as np
import os
import re
import sys

def read_ppr(fn):
    print('\n---read_ppr.py---\n')

    if not os.path.isfile(fn):
        print('ERROR: Input *.ppr file not found!')
        sys.exit()
    else:
        print('# Reading ' + fn + '...\n')

    # create a directory name based on the inputted ppr file
    ppr_dir = fn[0:-4] + '_ppr'

    print('# Saving waypoint data in ' + ppr_dir + '/...')

    # make the output directory if it doesn't exist
    if not os.path.exists(ppr_dir):
        os.makedirs(ppr_dir)

    lines_all = verbatim_lines(fn)

    # for later
    lines_anon = lines_all

    # zero indexed!
    # keys are unordered
    # to order keys, make a list of the keys and change the order
    # as necessary
    # keys are unique, values are not necessarily
    # spaces are no longer sacred, as not all waypoint plans have the same number of images
    # (but there is a max of 4 sequences (I4))

    lineidx = {
        'ALIAS',
        'REF IMAGE INFO',
        '[TRANSFORMATION',
        '[ANATOMY',
        '[MARKER',
        '[TRAJECTORY',
        'IMAGE INFO'
    }

    # let's go through each line, checking for these strings
    for l, line in enumerate(lines_all):
        for i in lineidx:
            line_parts = line.split(' ')

            # check for image/sequence info by the sequence ID (first part)
            if bool(re.search(('I'+'\d'), line_parts[0])):
                # confirm that this an imaging sequence info line (second part)
                line_parts_parts = line.split(':')
                line_parts_parts_parts = line_parts_parts[0].split('  ')
                if line_parts_parts_parts[1] == i:
                    # save the image ID
                    image_id = line_parts_parts_parts[0]
                    # save that info (Image Info: DimX DimY DimZ VszX VszY VszZ OriX OriY OriZ
                    # DataType(optional) BytOrder(opt) HdrSize(opt) Modality(opt) Min(opt) Max(opt))
                    line_parts_parts_parts_parts = line_parts_parts[1].split()
                    image_info = str(line_parts_parts_parts_parts[0:6])
                    image_info = image_info.replace(',', '')[1:-1]
                    image_info = image_info.replace("'", '')
                    # create a filename
                    image_info_fn = image_id + '_image_info'
                    prettysave(image_info, image_info_fn, ppr_dir)

            # checking for transformations
            if line_parts[0] == i:
            # let's confirm that this is a transformation matrix by checking if I*->REF is present
                if bool(re.search(('I'+'\d'), line_parts[1])):
                    # save the image ID
                    line_parts_parts = line_parts[1].split('->')
                    image_id = line_parts_parts[0]
                    # now let's actually concatenate the transform matrix
                    transform = str([lines_all[l+1], lines_all[l+2], lines_all[l+3], lines_all[l+4]])
                    transform = transform.replace(',', '')[1:-1]
                    transform = transform.replace("'", '')
                    # let's make a filename using the ID, and save it
                    transform_fn = image_id + '_transform.1D'
                    prettysave(transform, transform_fn, ppr_dir)

            # or, this could be some AC-PC/anatomy, marker, or trajectory info
            line_parts = line.split(']')
            if line_parts[0] == i:
                line_parts_parts = line.split('// ')
                # AC, PC, mid slice
                if line_parts_parts[1] == 'AC, PC, Mid-plane point in patient coordinates':
                    # parse the line for AC x,y,z values...
                    # also sample the whole line (AC coordinate) to save to the ppr/ directory
                    anatidx = {0:'ac', 1:'pc', 2:'midsag'}
                    key = dict.fromkeys(anatidx)
                    for key in anatidx:
                        anat_line = lines_all[l+1+key]
                        anat_fn = anatidx[key] + '_coord'
                        prettysave(anat_line, anat_fn, ppr_dir)

                    ac_val = np.loadtxt(os.path.join(ppr_dir, 'ac_coord'))
                    pc_val = np.loadtxt(os.path.join(ppr_dir, 'pc_coord'))
                    # calculate the mid-commissural point (no longer saved to disk),
                    # used for determining left and right trajectories/anchors
                    mc_val = (ac_val + pc_val)/2

                    # calculate the euclidean AC-PC distance
                    euclid_dist = np.linalg.norm(ac_val - pc_val)
                    euclid_dist_fn = 'euclid_dist'
                    prettysave(euclid_dist, euclid_dist_fn, ppr_dir)

                # let's get those markers (anchor screws) - screw base, then surgical platform anchor
                if line_parts_parts[1] == 'one marker set per line, base (anchor) followed by optional tip (marker) in patient coordinates':
                    screwidx = [1, 2, 3, 4]
                    for screw in screwidx:
                        try:
                            screw_line = lines_all[l+screw]
                            screw_vals = screw_line.split()
                            
                            # left-right test (if screw_base_x > mc_x, it's right)
                            if float(screw_vals[0]) > mc_val[0]:
                                lr_screw = 'r'
                            else:
                                lr_screw = 'l'

                            # anterior-posterior test (if screw_base_y > mc_y, it's posterior)
                            if float(screw_vals[1]) > mc_val[1]:
                                ap_screw = 'p'
                            else:
                                ap_screw = 'a'

                            screw_base = str(screw_vals[0:3]).replace(',', '')[1:-1]
                            screw_base = screw_base.replace("'", '')
                            screw_base_fn = lr_screw + '_' + ap_screw + '_screw_base'
                            prettysave(screw_base, screw_base_fn, ppr_dir)

                            screw_frame = str(screw_vals[3:6]).replace(',', '')[1:-1]
                            screw_frame = screw_frame.replace("'", '')
                            screw_frame_fn = lr_screw + '_' + ap_screw + '_screw_frame'
                            prettysave(screw_frame, screw_frame_fn, ppr_dir)

                        #in case there are less than 4 anchor screws
                        except IndexError:
                            print('no coordinates for screw', screw)

                #obtain trajectory target and entry points
                if line_parts_parts[1] == 'one trajectory per line, target point followed by optional entry point in patient coordinates and id':
                    trajidx = [1, 2]
                    for traj in trajidx:
                        try:
                            traj_line = lines_all[l+traj]
                            traj_vals = traj_line.split()
                            #left-right test (if traj_target_x > mc_x, it's right)
                            if float(traj_vals[0]) > mc_val[0]:
                                lr_traj = 'r'
                            else:
                                lr_traj = 'l'

                            traj_target = str(traj_vals[0:3]).replace(',', '')[1:-1]
                            traj_target = traj_target.replace("'", '')
                            traj_target_fn = lr_traj + '_traj_target'
                            prettysave(traj_target, traj_target_fn, ppr_dir)

                            traj_entry = str(traj_vals[3:6]).replace(',', '')[1:-1]
                            traj_entry = traj_entry.replace("'", '')
                            traj_entry_fn = lr_traj + '_traj_entry'
                            prettysave(traj_entry, traj_entry_fn, ppr_dir)
       
                        except IndexError:
                            print('no coordinates for traj', traj)

def verbatim_lines(f):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]

    return lines

# saves the output to ppr/ directory
def prettysave(item, fn, ppr_dir):
    dir_path = os.path.join(ppr_dir, fn)
    if not os.path.exists(dir_path):
        with open(dir_path, 'w') as out:
                out.write("{}\n".format(item))
    else:
        print(fn)
        print('# File already exists, will not overwrite.')

if __name__ == '__main__':
    parser = ap.ArgumentParser(description='This script extracts image orientation/transformation/voxel \
        dimension information, coordinates of anatomical features (AC, PC, midsagittal point), \
        and coordinates of trajectories (target point, entry point, fiducial screws) from the waypoint \
        PPR file. It saves all information as plain text files to disk in a directory with the same name \
        as the input file. Make sure the PPR file was anonymized with anon_ppr.py before proceeding')

    parser.add_argument('filename')

    args = parser.parse_args()

    read_ppr(args.filename)

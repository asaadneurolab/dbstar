#!/usr/bin/env python

# last update (2017-11-05): improved readability, now using *.nii.gz volumes

# atlas_align.py - calls AFNI's 'auto_warp.py' non-linear registration function to register it to the
# TT_N27 volume/atlas including in the AFNI installation (assuming it is installed in /usr/local/abin)

import argparse as ap
from subprocess import call
import sys

def atlas_align(fn, atlas_vol, out_dir, workhard):
    print('\n---atlas_align.py---\n')

    # check if there is input
    if fn is None:
        print('ERROR: No input T1 volume! (-t1_vol)')
        sys.exit()

    # define the warp-target-atlas volume
    if atlas_vol is None:
        atlas_vol = '/usr/local/abin/TT_N27+tlrc'

    # define the output directory
    if out_dir is None:
        out_dir = 'tt_warp'

    # engage warp drive (perform the non-linear warp, can take ~1 hour)
    if workhard is True:
        print('# Making auto_warp.py work hard...\n')
        call(['auto_warp.py',
            '-input', fn,
            '-base', atlas_vol,
            '-skull_strip_input', 'yes',
            '-output_dir', out_dir,
            '-ex_mode', 'quiet',
            '-qworkhard', '0', '7'])
    else:
        print('# Performing non-linear registration to atlas...\n')
        call(['auto_warp.py',
            '-input', fn,
            '-base', atlas_vol,
            '-skull_strip_input', 'yes',
            '-output_dir', out_dir,
            '-ex_mode', 'quiet'])

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script non-linearly registers a T1 volume to the \
        TT_N27 AFNI reference/atlas volume')

    parser.add_argument('-t1_vol', action='store',
        help='a T1 nifti volume brought to the ACPC/MCP space by waypoint_convert.py \
            (e.g. t1_pre_refit_mcp.nii)')

    parser.add_argument('-atlas_vol', action='store',
        help='a filepath leading to the same atlas at another location/another atlas to use for \
            non-linear registration (default: /usr/local/abin/TT_N27+tlrc)')

    parser.add_argument('-out_dir', action='store',
        help='specify the output directory for warp matrices/volumes (default: tt_warp)')

    parser.add_argument('-workhard', action='store_true', default='False',
        help='flag which makes non-linear fitting (3dQwarp vis-a-vis auto_warp.py) perform \
            more fitting iterations - use if default settings did not produce a satifactory fit')

    args = parser.parse_args()

    atlas_align(args.t1_vol, args.atlas_vol, args.out_dir, args.workhard)

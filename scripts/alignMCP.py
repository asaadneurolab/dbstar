#!/usr/bin/env python

# last change (2017-11-05): improved readability

# alignMCP.py - Calculate the AC-PC geometry of the subject, and create rigid
# transformation matrix for coordinate transformation relative to MC.
# Adapted from Dr. Andrei Barborica's alignMCP.m

import align3D
import argparse as ap
import numpy as np
import os

def alignMCP(ac, pc, midsag):

    euclid_dist = np.linalg.norm(ac - pc)

    # add a 1 to make them homogenous coordinates
    ac_h = np.array(np.append(ac, 1)).transpose()
    pc_h = np.array(np.append(pc, 1)).transpose()
    midsag_h = np.array(np.append(midsag, 1)).transpose()

    # align the AC-PC line
    TACPC = align3D.align3D(np.array([pc, ac]),
        np.array([ [0, -0.5*euclid_dist, 0], [0, 0.5*euclid_dist, 0] ]))

    # Find out new midsag coordinates using the TACPC trasformation matrix
    midsag_origin = np.dot(TACPC, midsag_h)
    # Check if a rotation is required around the ACPC line
    if midsag_origin[0] == 0:
        # no additional rotation required
        rot = TACPC
    else:
        # Look only at the projection on the XZ plane
        MCPMP = np.sqrt(midsag_origin[0]**2 + midsag_origin[2]**2)
        # Next, align the new MP point location with the vertical axis
        TMP = align3D.align3D(np.array([[0, 0, 0], [midsag_origin[0], 0, midsag_origin[2]]]),
            np.array([[0, 0, 0], [0, 0, MCPMP]]))
        rot = np.dot(TMP, TACPC)

    return rot


if __name__ == '__main__':

    parser = ap.ArgumentParser(description='Calculate the AC-PC geometry of the \
        subject, and create rigid transformation matrix for coordinate transformation \
        relative to MC. Adapted from Dr. Andrei Barborica"s alignMCP.m')

    parser.add_argument('ppr_dir')

    args = parser.parse_args()

    # load data from read_ppr.py
    ac = np.loadtxt(os.path.join(args.ppr_dir, 'ac_coord'))
    pc = np.loadtxt(os.path.join(args.ppr_dir, 'pc_coord'))
    midsag = np.loadtxt(os.path.join(args.ppr_dir, 'midsag_coord'))

    alignMCP(ac, pc, midsag)

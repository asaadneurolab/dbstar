#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes

# surf_and_clip.py - formats @eproc output to a depth-coordinate-friendly format 

import argparse as ap
from subprocess import call

def surf_and_clip(track1, track2, track3, track4):
    print('\n---surf_and_clip.py---\n')

    # for each track...
    niftis = track1, track2, track3, track4
    for n in niftis:
        if n is not None:
            # remove .nii extension
            n = n.split('.')[0]

            # calculate the center-of-mass of the nifti volume
            # (important for trajectory selection later)
            cm_fn = n + '.com.1D'
            with open(cm_fn, 'w') as f:
                call(['3dCM', n + '.nii.gz'],
                    stdout=f)

            # reconstruct the niftis as smoothed surfaces (giftis)
            call(['IsoSurface',
                '-isoval', '1',
                '-Tsmooth', '0.1', '100',
                '-o_gii', n,
                '-input', n + '.nii.gz'])

            # calculate the depth of each node along the principal axis
            call(['ConvertSurface',
                '-i', n + '.gii',
                '-node_depth', n])

            # now calculate the nodes that are <=35 mm above the bottom of the track
            mmm_fn = n + '.mmm.1D'        
            with open(mmm_fn, 'w') as f:
                call(['1deval',
                    '-a', n + '.pcdepth.1D.dset[2]',
                    '-expr','a*step(35-a)'],
                    stdout=f)

            sel_fn = n + '.sel.temp.1D'
            with open(sel_fn, 'w') as f:
                call(['1dcat',
                    n + '.pcdepth.1D.dset[0]',
                    mmm_fn],
                    stdout=f)

            # and now clip all nodes >35 mm above the bottom of the track
            call(['SurfPatch',
                '-i', n + '.gii',
                '-input',sel_fn, '0', '1',
                '-patch2surf',
                '-out_type', 'gii',
                '-prefix', n + '.clp',
                '-node_depth', n + '.clp'])

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script formats @eproc output to a \
        depth-coordinate-friendly format')

    parser.add_argument('-track1', action='store',
        help='the nifti volume of the first electrode track')

    parser.add_argument('-track2', action='store',
        help='the nifti volume of the second electrode track')

    parser.add_argument('-track3', action='store',
        help='the nifti volume of the third electrode track')

    parser.add_argument('-track4', action='store',
        help='the nifti volume of the fourth electrode track')

    args = parser.parse_args()

    surf_and_clip(args.track1, args.track2, args.track3, args.track4)

#!/usr/bin/env python

# last change (2017-11-16): minor fix for terminal output

# combine_spheres_subject.py - creates spheres for each atlas-warped recording,
# final, physiology electrode placement coordinate for a patient

import argparse as ap
import os
from subprocess import call
import sys

def combine_spheres_subject(track1, track2, track3, track4, atlas_dir,
    record_spheres, final_spheres, phys_spheres, combine_all, macro):

    print('\n---combine_spheres_subject.py---\n')

    # define atlas directory if not specified
    if atlas_dir is None:
        atlas_dir = 'tt_warp'

    # are we calculating recording depth speres?
    if record_spheres is True:
        print('# Combining spheres for recording locations...')

    # are we calculating final depth spheres?
    if final_spheres is True:
        print('# Combining spheres for final electrode locations...')

    # are we calculating physiology value spheres?
    if phys_spheres is True:
        print('# Combining physiology value spheres...')

    # are we combining all our spheres?
    if combine_all is True:
        print('# Combining the resulting combined spheres...')

    # search for macro electrodes, if indicated
    if macro is True:
        print('# Searching for macro electrode coordinates only')

    # for each track...
    niftis = track1, track2, track3, track4

    # are these the right track files? check if they exist,
    #or ifthey aren't *.clp.gii files
    for n in niftis:
        if n is not None:
            n_fn = os.path.join('./', n[0])
            if os.path.isfile(n_fn) is False:
                print('ERROR: {} does not exist.'.format(n[0]))
                sys.exit()
            if not n[0].endswith('.clp.gii'):
                print('ERROR: {} does not end with *.clp.gii'.format(n[0]))
                sys.exit()
            # check if the final trajectory text file exists
            final_traj_fn = os.path.join('./', n[1])
            if os.path.isfile(final_traj_fn) is False:
                print('ERROR: {} does not exist.'.format(n[1]))
                sys.exit()

    # for each track...
    for n in niftis:
        if n is not None:
            go_combine_all = False
            track_id = n[0][0:-8]

            if record_spheres is True:
                # combine spheres for recording locations

                if macro is False:
                    record_track_id = track_id + '_record'
                elif macro is True:
                    record_track_id = track_id + '_record_macro'

                print('\n# Combining recording location spheres for {}...'.format(track_id))

                temp_sphere_count = 0
                sphere_list = []
                multi_spheres = 1
                for file in os.listdir(atlas_dir):

                    if macro is False:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.nii.gz') \
                            and 'macro' not in file and '_0.0_' not in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1
                    elif macro is True:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.nii.gz') \
                            and 'macro' in file and '_0.0_' not in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1

                    if temp_sphere_count == 26:
                        common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                            record_track_id, go_combine_all)
                        multi_spheres += 1
                        temp_sphere_count = 0
                        sphere_list = []

                if temp_sphere_count != 0:
                    common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                        record_track_id, go_combine_all)
                else:
                    print('# No matching recording spheres detected, no combined sphere will be')
                    print('  generated. Did you run warp_depth_coords.py with "-make_spheres" yet?')

            # save the correct final recording location sphere
            if final_spheres is True:

                if macro is False:
                    final_track_id = track_id + '_final'
                elif macro is True:
                    final_track_id = track_id + '_final_macro'

                final_t = verbatim_lines_traj(n[1])[0]
                final_traj_test = '_' + final_t.lower() + '_'

                print('\n# Creating final location sphere (along track {}) for {}...'.format(final_t.lower(), track_id))

                temp_sphere_count = 0
                sphere_list = []
                multi_spheres = 1
                for file in os.listdir(atlas_dir):

                    if macro is False:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.nii.gz') \
                            and 'macro' not in file and '_0.0_' in file and final_traj_test in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1
                    elif macro is True:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.nii.gz') \
                            and 'macro' in file and '_0.0_' in file and final_traj_test in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1

                    if temp_sphere_count == 26:
                        common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                            final_track_id, go_combine_all)
                        multi_spheres += 1       
                        temp_sphere_count = 0
                        sphere_list = []         

                if temp_sphere_count != 0:
                    common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                        final_track_id, go_combine_all)
                else:
                    print('# No matching final location spheres detected, no combined sphere will be')
                    print('  generated. Did you run warp_depth_coords.py with "-make_spheres" yet?')

            # save the correct phys recording location sphere
            if phys_spheres is True:

                if macro is False:
                    phys_track_id = track_id + '_phys'
                elif macro is True:
                    phys_track_id = track_id + '_phys_macro'

                print('\n# Creating physiology location spheres for ...'.format(track_id))

                temp_sphere_count = 0
                sphere_list = []
                multi_spheres = 1
                for file in os.listdir(atlas_dir):

                    if macro is False:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.phys.nii.gz') \
                            and 'macro' not in file and '_0.0_' not in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1
                    elif macro is True:
                        if file.startswith(track_id) and file.endswith('track.iwarp.coord.sphere.phys.nii.gz') \
                            and 'macro' in file and '_0.0_' not in file:
                            sphere_list.append(file)
                            temp_sphere_count += 1 

                    if temp_sphere_count == 26:
                        common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                            phys_track_id, go_combine_all)
                        multi_spheres += 1       
                        temp_sphere_count = 0
                        sphere_list = []         

                if temp_sphere_count != 0:
                    common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                        phys_track_id, go_combine_all)
                else:
                    print('# No matching phys location spheres detected, no combined sphere will be')
                    print('  generated. Did you run warp_depth_coords.py with "-make_spheres" yet?')

            if combine_all is True:
                go_combine_all = True

                sphere_list=[]
                temp_sphere_count = 0

                if record_spheres is True:
                    all_track_id = record_track_id
                elif final_spheres is True:
                    all_track_id = final_track_id
                elif phys_spheres is True:
                    all_track_id = phys_track_id

                for cs in range(1, multi_spheres):
                    sphere_list.append(all_track_id + '_sphere' + str(cs) + '.nii.gz')
                    temp_sphere_count += 1

                common_sphere(sphere_list, temp_sphere_count, multi_spheres, atlas_dir,
                    phys_track_id, go_combine_all)

def common_sphere(sphere_list, sphere_count, multi_spheres, atlas_dir,track_id,go_combine_all):
        abc = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
        comm = '3dcalc'
        expr = ' -expr "('
        if go_combine_all is True:
            prefix = '" -prefix ' + atlas_dir + '/' + track_id + '_all.nii.gz'
        else:
            prefix = '" -prefix ' + atlas_dir + '/' + track_id + '_sphere' + str(multi_spheres) + '.nii.gz'

        for s, fl in enumerate(sphere_list):
            comm = comm + ' -' + abc[s] + ' ' + atlas_dir + '/' + fl
            if s is 0:
                expr = expr + abc[s]
            else:
                expr = expr + '+' + abc[s]

        comm = comm + expr + ')' + prefix
        # I would like to suppress output, but the non-overwrite warnings are too important
        # to miss (to suppress completely, add 'stderr=fnull' to call
        with open(os.devnull, 'w') as fnull:    
            call(str(comm), shell=True)                

        # save number of spheres used here
        if go_combine_all is True:
            nspheres_fn = track_id + '_all_count'
            prettysave(sphere_count, nspheres_fn, atlas_dir)
        else:
            nspheres_fn = track_id + '_sphere' + str(multi_spheres) + '_count'
            prettysave(sphere_count, nspheres_fn, atlas_dir)

def verbatim_lines_traj(f):
    with open(f) as f_in:
        # list comprehension, sparing empty new-line characters
        # (should read exactly like the source text)
        lines = (line.rstrip() for line in f_in)
        lines = [line for line in lines]
        traj = str(lines[0])
        verbatim_traj = traj.split()

    return verbatim_traj

# saves the output to the output directory
def prettysave(item, fn, out_dir):
    dir_path = os.path.join(out_dir, fn)
    if not os.path.exists(dir_path):
        with open(dir_path, 'w') as out:
                out.write("{}\n".format(item))
    else:
        print(fn)
        print('# File already exists, will not overwrite.\n')

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script generates spheres (via 3dcalc) for each \
        atlas-warped recording and final electrode placement coordinate for each patient')

    parser.add_argument('-track1', action='store', nargs=2,
        help='the clipped gifti surface (*clp.gii) of the first electrode track, and its final trajectory info')

    parser.add_argument('-track2', action='store', nargs=2,
        help='the clipped gifti surface (*clp.gii) of the second electrode track, and its final trajectory info')

    parser.add_argument('-track3', action='store', nargs=2,
        help='the clipped gifti surface (*clp.gii) of the third electrode track, and its final trajectory info')

    parser.add_argument('-track4', action='store', nargs=2,
        help='the clipped gifti surface (*clp.gii) of the fourth electrode track, and its final trajectory info')

    parser.add_argument('-atlas_dir', action='store',
        help='output directory generated by atlas_align.py containing non-linear warp volumes and \
            transforms - also the output directory for warped coordinates (default: tt_warp)')

    parser.add_argument('-record_spheres', action='store_true', default=False,
        help='flag to combine the recording location spheres for each track')

    parser.add_argument('-final_spheres', action='store_true', default=False,
        help='flag to combine the final electrode spheres for each track')

    parser.add_argument('-phys_spheres', action='store_true', default=False,
        help='flag to combine the physiology value spheres for each track')

    parser.add_argument('-combine_all', action='store_true', default=False,
        help='flag to combine all generated combined spheres')

    parser.add_argument('-macro', action='store_true', default=False,
        help='flag that searches for macro-electrode, not micro-electrode, recording sites')

    args = parser.parse_args()

    combine_spheres_subject(args.track1, args.track2, args.track3, args.track4, args.atlas_dir,
        args.record_spheres, args.final_spheres, args.phys_spheres, args.combine_all, args.macro)

#!/usr/bin/env python

# last change (2017-11-28): added '-suffix' option for filtering spreadsheets

# combine_report_coords.py - consolidates the outputted spreadsheets from report_coords.py
# into one spreadsheet

# if your default python is python3.* -> install the pyexcel-ods3 package
# (https://github.com/pyexcel/pyexcel-ods3)
# if your default python is python2.7.* -> install the pyexcel-ods package
# (https://github.com/pyexcel/pyexcel-ods)

import argparse as ap
import os
import sys
from collections import OrderedDict

if sys.version_info[0] == 2:
    from pyexcel_ods import save_data
    from pyexcel_ods import get_data
elif sys.version_info[0] == 3:
    from pyexcel_ods3 import save_data
    from pyexcel_ods3 import get_data
else:
    print('ERROR: Unrecognized python version...cannot load pyexcel_ods*')
    sys.exit()

def combine_report_coords(output, suffix):

    print('\n---combine_report_coords.py---\n')

    # We haven't found a header yet
    header = False

    # initialize some spreadsheet things
    if output is None:
        out_sheet_fn = 'all_subjs.ods'
    elif output[-4:] == '.ods':
        out_sheet_fn = output
    else:
        out_sheet_fn = output + '.ods'

    if suffix is not None:
        print('# Only selecting spreadsheets with the *{}.ods suffix...\n'.format(suffix))
        ods_suffix = suffix + '.ods'
    else:
        ods_suffix = '.ods'

    # let's walk through each subject directory
    for sj_dir in next(os.walk('.'))[1]:
        # double-check that this subject directory starts with 's', indicating our patient IDs
        if sj_dir == 'dbstar':
            # let's make a list of all the *.ods spreadsheets in this directory
            for fn in os.listdir(sj_dir):
                if fn.endswith(ods_suffix):
                    print('# Loading ' + os.path.join(sj_dir, fn) + '...')
                    temp_ods = get_data(os.path.join(sj_dir, fn))
                    temp_sheet = temp_ods.get('Sheet 1')
                    # let's make sure we grab a header
                    if header is False:
                        out_sheet = OrderedDict({'Sheet 1': [temp_sheet[0]]})
                        header=True

                    # extract the output sheet data, add every non-header row from the imported
                    #  ods file, create a new output spreadsheet
                    new_rows = out_sheet.get('Sheet 1')
                    for row in range(1, len(temp_sheet)):
                        new_rows.extend([temp_sheet[row]])
                    
                    out_sheet.update({'Sheet 1': new_rows})

    # save the final spreadsheet
    if os.path.isfile(out_sheet_fn):
        print(out_sheet_fn)
        print('# File already exists, will not overwrite.')
    else:
        print('##Final spreadsheet saved to ./' + out_sheet_fn)
        save_data(out_sheet_fn, out_sheet) 

if __name__ == '__main__':

    parser = ap.ArgumentParser(description='# This script consolidates the output spreadsheets from \
        report_coords.py into one spreadsheet - you need to install the pyexcel-ods3 (python3.*) or \
        pyexcel-ods (python2.7.*) package (https://github.com/pyexcel/pyexcel-ods3). \
        Run in parent directory containing each subject directory')

    parser.add_argument('-output', action='store',
        help='filename of output spreadsheet (default: all_subjs.ods)')

    parser.add_argument('-suffix', action='store',
        help='specify a file suffix (file_[suffix].ods) to select specific spreadsheets')

    args = parser.parse_args()

    combine_report_coords(args.output, args.suffix)

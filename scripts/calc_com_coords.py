#!/usr/bin/env python

# last change (2017-11-05): improved readability, now using *.nii.gz volumes

# calc_com_coords.py - determines where depth coordinates are located
# within a patient's segmented T1 volume and atlas volume target masks
# using their respective center-of-mass

import argparse as ap
import numpy as np
import os
from subprocess import call
import sys

def calc_com_coords(coord_dir, t1_vol, seg_vol, test_atlas, atlas_vol,
    atlas_dir, out_dir, anat_test, save_masks):

    print('\n---calc_com_coords.py---\n')

    # define coordinate directory if not specified
    if coord_dir is None:
        coord_dir = 'coords'

    # define the t1 volume
    if t1_vol is None:
        t1_vol = 't1_pre_refit_mcp.nii'
    
    # define the segmentation volume
    if seg_vol is None:
        seg_vol = './aparc+aseg_rank.nii'

    # check if a new test atlas has been specified
    if test_atlas is None:
        test_atlas = 'TT_Daemon'

    # define the warp-target-atlas volume
    if atlas_vol is None:
        atlas_vol = '/usr/local/abin/TT_N27+tlrc'

    # define atlas directory if not specified
    if atlas_dir is None:
        atlas_dir = 'tt_warp'
        
    # make the output directory if it doesn't exist
    if out_dir is None:
        out_dir = 'com'

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
        
    # make sure an anatomical target is specified
    if anat_test is None:
        print('ERROR: No anatomical target (-anat_test) specified!')
        print('       Choices include VL, STN, GPi, GPe...')
        sys.exit()
    else:
        print('# Testing coordinates against the center-of-mass')
        print('  of {}...\n'.format(anat_test))

    # check if we are saving roi volumes
    if save_masks is True:
        print('# Saving intermediate ROI mask volumes and')
        print('  their reference volumes (T1, TT_N27)...\n')

    # obtain freesurfer and atlas values
    atlas_str, atlas_out, fs_val, fs_out = check_anat(anat_test)
        
    # calculate centers of mass for anat_test volume
    # in freesurfer and atlas space

    print('# Creating ROI masks and calculating the center-of-mass')
    print('  coordinates for Freesurfer targets...\n')

    ## freesurfer (left first)
    l_fs_com = com_calc(fs_out[0], out_dir, fs_val[0], seg_vol,
        test_atlas, t1_vol, atlas_vol, save_masks, 'fs')
    # then right
    r_fs_com = com_calc(fs_out[1], out_dir, fs_val[1], seg_vol,
        test_atlas, t1_vol, atlas_vol, save_masks, 'fs')

    print('# Creating ROI masks and calculating the center-of-mass')
    print('  coordinates for atlas targets...\n')

    # same for atlas volumes (left, then right)
    l_atlas_com = com_calc(atlas_out[0], out_dir, atlas_str[0], seg_vol,
        test_atlas, t1_vol, atlas_vol, save_masks, 'atlas')
    r_atlas_com = com_calc(atlas_out[1], out_dir, atlas_str[1], seg_vol,
        test_atlas, t1_vol, atlas_vol, save_masks, 'atlas')
    
    # Now load all native depth coordinates
    print('# Calculating the difference between native depth coordinates')
    print('  and the Freesurfer target center-of-mass coordinate...\n')
    
    coord_calc(coord_dir, l_fs_com, r_fs_com, out_dir)
    
    # repeat for all atlas-warped coordinates
    print('# Calculating the difference between atlas-warped depth coordinates')
    print('  and the atlas target center-of-mass coordinate...\n')
    
    coord_calc(atlas_dir, l_atlas_com, r_atlas_com, out_dir)
            
def com_calc(roi_label, out_dir, roi_val, seg_vol, test_atlas,
    t1_vol, atlas_vol, save_masks, type):

        # first create the volume
        out_vol = os.path.join(out_dir, roi_label + '.nii.gz')
        
        # freesurfer
        if 'fs' in type:
            if not os.path.exists(out_vol):
                expr = 'equals(a,{})'.format(str(roi_val))
                with open(os.devnull, 'w') as fnull:
                    call(['3dcalc',
                        '-a', seg_vol,
                        '-expr', expr,
                        '-prefix', out_vol],
                        stdout=fnull, stderr=fnull)
                    
        # atlas (TT-daemon based)
        elif 'atlas' in type:
            if not os.path.exists(out_vol):
                atlas_expr = test_atlas + ':' + roi_val
                with open(os.devnull, 'w') as fnull:
                    call(['whereami',
                        '-mask_atlas_region', atlas_expr,
                        '-prefix',out_vol],
                        stdout=fnull, stderr=fnull)

        # then calculate the center-of-mass of the resulting volume
        com_fn = roi_label + '_com'
        com_out = os.path.join(out_dir, com_fn)
        if os.path.exists(out_vol):
            if not os.path.exists(com_out):
                with open(com_out, 'w') as f:
                    call(['3dCM', out_vol], stdout=f)

        # if segmentation volume doesn't exist, output -999 (code for error)
        else:
            print('ERROR: {} does not exist! Outputting dummy')
            print('       center-of-mass coordinates in its place!')
            print('       [-999 -999 -999]'.format(out_vol))
            with open(com_out,'w') as f:
                call(['echo','-999 -999 -999'],stdout=f)

        final_com = np.loadtxt(com_out)
                    
        # ...and add the reference volume for quality-checking
        # (if indicated by -save_masks)
        if save_masks is True:
            if 'fs' in type:
                t1_out = os.path.join(out_dir, t1_vol)
                if not os.path.exists(t1_out):
                    with open(os.devnull, 'w') as fnull:
                        call(['3dcopy',
                            t1_vol, t1_out],
                            stdout=fnull, stderr=fnull)

            elif 'atlas' in type:
                with open(os.devnull, 'w') as fnull:
                    call(['3dcopy',
                        atlas_vol, out_dir + '/'],
                        stdout=fnull, stderr=fnull)
        else:
            # delete the unwanted masks if -save_masks is not specified
            call(['rm', out_vol])

        return final_com
            
def coord_calc(c_dir, l_com, r_com, out_dir):
    for file in os.listdir(c_dir):
        if file.endswith('.coord'):
            coord_fn = os.path.join(c_dir, file)

            # check if the coordinate file is empty, if not load it
            if os.stat(coord_fn).st_size == 0:
                print('# {} is empty...skipping'.format(file))
            else:
                test_coord = np.loadtxt(coord_fn)
                
                # determine the proper com coord to test
                # left coordinates always have a positive x-value
                if test_coord[0] > 0:
                    diff_coord = test_coord - l_com
                elif test_coord[0] < 0:
                    diff_coord = test_coord - r_com
                else:
                    print('# ERROR: {} does not have a proper')
                    print('  x-coordinate value...skipping'.format(file))
            
                # calculate the euclidean distance
                diff_coord = np.append(diff_coord,np.linalg.norm(diff_coord))
                diff_coord_out = str(diff_coord)[1:-1]
                # save results to out_dir, using same name
                # as native depth coordinate
                diff_coord_fn = os.path.join(out_dir, (file[0:-6] + '.com.coord'))
                prettysave(diff_coord_out, diff_coord_fn)
            
# saves the output to the current directory
def prettysave(item, fn):
    if not os.path.exists(fn):
        with open(fn, 'w') as out:
            out.write("{}\n".format(item))
    else:
        print(fn)
        print('# File already exists, will not overwrite.\n')
         
def check_anat(anat_test):
# dictionary of possible regions-of-interest to test...can easily expand/be made into a larger dictionary
    atlas_str = None

    if 'VL' in anat_test:
        atlas_str = ['Left Ventral Lateral Nucleus','Right Ventral Lateral Nucleus']
        atlas_out = ['l_vim_atlas','r_vim_atlas']
        fs_val = [7,27]
        fs_out = ['l_thal_fs','r_thal_fs']
    if 'STN' in anat_test:
        atlas_str = ['Left Subthalamic Nucleus','Right Subthalamic Nucleus']
        atlas_out = ['l_stn_atlas','r_stn_atlas']
        fs_val = [18,34]
        fs_out = ['l_vdc_fs','r_vdc_fs']
    if 'GPi' in anat_test:
        atlas_str = ['Left Medial Globus Pallidus','Right Medial Globus Pallidus']
        atlas_out = ['l_gpi_atlas','r_gpi_atlas']
        fs_val = [10,30]
        fs_out = ['l_pal_fs','r_pal_fs']
    if 'GPe' in anat_test:
        atlas_str = ['Left Lateral Globus Pallidus','Right Lateral Globus Pallidus']
        atlas_out = ['l_gpe_atlas','r_gpe_atlas']
        fs_val = [10,30]
        fs_out = ['l_pal_fs','r_pal_fs']

    # Make sure we have ROI values to work with
    if atlas_str is None:
        print('ERROR: -anat_test did not match any pre-programmed')
        print('regions-of-interest (VL, STN, GPi, GPe))')
        sys.exit()
    else:
        return atlas_str, atlas_out, fs_val, fs_out
        
if __name__ == '__main__':

    parser = ap.ArgumentParser(description='This script determines where depth \
        coordinates are located within a patient- and atlas-specific anatomical \
        segmentation center of masses, outputting to plaintext files')

    parser.add_argument('-coord_dir', action='store',
        help='directory containing coordinates to test (typically generated \
            by calc_depth_coords.py) (default: coords)')

    parser.add_argument('-t1_vol', action='store',
        help='a T1 nifti volume brought to the ACPC/MCP space by \
            waypoint_convert.py (e.g. t1_pre_refit_mcp.nii)')

    parser.add_argument('-seg_vol', action='store',
        help='the freesurfer-segmented volume (typically generated by \
            t1_acpc_seg.py) to test coordinates against (default: \
            ./aparc+aseg_rank.nii)')

    parser.add_argument('-test_atlas', action='store',
        help='specify a different atlas for AFNI/whereami to check coordinate \
            location (default: TT_Daemon)')

    parser.add_argument('-atlas_vol', action='store',
        help='a filepath leading to the same atlas at another location/another \
            atlas to use for non-linear registration (default: \
            /usr/local/abin/TT_N27+tlrc)')

    parser.add_argument('-atlas_dir', action='store',
        help='output directory generated by atlas_align.py containing \
            non-linear warp volumes and transforms - also the output directory \
            for warped coordinates (default: tt_warp)')

    parser.add_argument('-out_dir', action='store',
        help='specify the output directory for center-of-mass coordinates \
            (default: com)')

    parser.add_argument('-anat_test', action='store',
        help='label of roi you would like to calculate center of mass for \
            (e.g. VL, STN, GPi, GPe')

    parser.add_argument('-save_masks', action='store_true', default=False,
        help='flag to ensure roi mask volumes and their reference volumes \
            (T1, TT_N27) are not deleted')

    args = parser.parse_args()

    calc_com_coords(args.coord_dir, args.t1_vol, args.seg_vol, args.test_atlas,
        args.atlas_vol, args.atlas_dir, args.out_dir, args.anat_test, args.save_masks)

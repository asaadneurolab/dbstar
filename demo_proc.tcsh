#!/bin/tcsh

# last update (v1.0.0a; 2018-02-08): minor documentation changes

# tcsh shell script for using DBStar processing scripts
# (for use with sample data)

# set your directory containing repository scripts
set script_dir = ./scripts

# what is your ppr (i.e. patient ID)?
set ppr_fn = 'demo'

# load waypoint ppr info
$script_dir/read_ppr.py ${ppr_fn}_anon.ppr

# convert raw volumes to waypoint MCP/ACPC orientation
$script_dir/waypoint_convert.py \
    -preop_ct ct_pre.nii \
    -preop_t1 t1_pre.nii \
    -extra1 ct_intra.nii \
    -extra2 t1_post.nii \
    -intraop_ct extra1 \
    -ppr_dir ${ppr_fn}_anon_ppr

## QC waypoint conversion for appropriate registration/ACPC coordinate values
## (0,0,0 = MC point: remember, the preop CT is waypoint's reference volume)

## run the next five functions overnight

# invert the post-op T1 so the 'electrodes' can be segmented out
$script_dir/intensity_invert.py \
    -t1_vol t1_post_refit_mcp.nii.gz \
    -skull_strip

# segment out some electrodes
$script_dir/@eproc_dbstar t1_post_refit_mcp_brain_inv.nii.gz
$script_dir/@eproc_dbstar ct_intra_refit_mcp.nii.gz

# freesurfer-segment the t1 volume (resuming with recon-all -make all -sd ./ -s t1_pre_refit_mcp_u)
$script_dir/t1_acpc_seg.py -t1_vol t1_pre_refit_mcp.nii.gz

# register the t1 to talairach atlas
$script_dir/atlas_align.py -t1_vol t1_pre_refit_mcp.nii.gz

# QC each @eproc segmentation, and create appropriate tracks using the manual method
# (manual recommended for post-op T1s or lower-quality CT images;
#  automatic recommended only for high-resolution CT images)

# manual for intra-op CTs
cd CT_eseg.ct_intra_refit_mcp.nii.gz/

3dcalc -a ct.clst3+orig -expr 'equals(a,2)' -prefix left_track_ct.nii.gz

cp left_track_ct.nii.gz ../.

3dcalc -a ct.clst3+orig -expr 'equals(a,1)' -prefix right_track_ct.nii.gz

cp right_track_ct.nii.gz ../.

cd ..

# manual for post-op T1s
cd CT_eseg.t1_post_refit_mcp_brain_inv.nii.gz/

3dcalc -a ct.clst2+orig -expr 'equals(a,12)' -prefix left_track_t1.nii.gz

cp left_track_t1.nii.gz ../.

3dcalc -a ct.clst2+orig -expr 'equals(a,9)' -prefix right_track_t1.nii.gz

cp right_track_t1.nii.gz ../.

cd ..

# prep electrode tracks for coordinate calculation
$script_dir/surf_and_clip.py \
    -track1 left_track_ct.nii.gz \
    -track2 left_track_t1.nii.gz \
    -track3 right_track_ct.nii.gz \
    -track4 right_track_t1.nii.gz

# calculate coordinates for intraop_ct tracks

# note that image trajectory (-image_traj) and trajectories to calculate coordinates for (-trajs)
# are not the same...although the intraop_ct usually contains all Ben-gun tracks used for operative
# recordings, a medial trajectory was chosen later

# (if you want to calculate macroelectrode coordinates, add '-macro')

$script_dir/calc_depth_coords.py \
    -track left_track_ct.clp.gii \
    -ppr_dir ${ppr_fn}_anon_ppr \
    -depths l_depth \
    -final_depth l_final_depth \
    -trajs l_traj \
    -image_traj l_ct_traj \
    -intraop_ct \
    -intraop_ct_depth l_ct_depth

$script_dir/calc_depth_coords.py \
    -track right_track_ct.clp.gii \
    -ppr_dir ${ppr_fn}_anon_ppr \
    -depths r_depth \
    -final_depth r_final_depth \
    -trajs r_traj \
    -image_traj r_ct_traj \
    -intraop_ct \
    -intraop_ct_depth r_ct_depth

# calculate coordinates for postop_t1 tracks

# note that -image_traj is now *_final_traj, the final trajectory used
# ...as this is what is represented in the postop MR image

$script_dir/calc_depth_coords.py \
    -track left_track_t1.clp.gii \
    -ppr_dir ${ppr_fn}_anon_ppr \
    -depths l_depth \
    -final_depth l_final_depth \
    -trajs l_traj \
    -image_traj l_final_traj

$script_dir/calc_depth_coords.py \
    -track right_track_t1.clp.gii \
    -ppr_dir ${ppr_fn}_anon_ppr \
    -depths r_depth \
    -final_depth r_final_depth \
    -trajs r_traj \
    -image_traj r_final_traj

# freesurfer-test coordinates
$script_dir/test_fs_location.py \
    -coord_dir coords/ \
    -t1_vol t1_pre_refit_mcp.nii.gz

## TT-test coordinates
$script_dir/warp_depth_coords.py \
    -coord_dir coords/ \
    -make_spheres

# Check the resulting coordinate spheres, record in a spreadsheet

# report coords for left, then right tracks (STN = STN)
$script_dir/report_coords.py \
    -track left_track_ct.clp.gii \
    -trajs l_traj \
    -depths l_depth \
    -session_id l_session_id \
    -target L_STN_ct \
    -image_traj l_final_traj \
    -final_depth l_final_depth \
    -ref_image intraop_ct \
    -anat_test STN

$script_dir/report_coords.py \
    -track left_track_t1.clp.gii \
    -trajs l_traj \
    -depths l_depth \
    -session_id l_session_id \
    -target L_STN_t1 \
    -image_traj l_final_traj \
    -final_depth l_final_depth \
    -ref_image postop_t1 \
    -anat_test STN

$script_dir/report_coords.py \
    -track right_track_ct.clp.gii \
    -trajs r_traj \
    -depths r_depth \
    -session_id r_session_id \
    -target R_STN_ct \
    -image_traj r_final_traj \
    -final_depth r_final_depth \
    -ref_image intraop_ct \
    -anat_test STN

$script_dir/report_coords.py \
    -track right_track_t1.clp.gii \
    -trajs r_traj \
    -depths r_depth \
    -session_id r_session_id \
    -target R_STN_t1 \
    -image_traj r_final_traj \
    -final_depth r_final_depth \
    -ref_image postop_t1 \
    -anat_test STN

# center of mass analysis

$script_dir/calc_com_coords.py -anat_test STN

$script_dir/report_coords.py \
    -track left_track_ct.clp.gii \
    -trajs l_traj \
    -depths l_depth \
    -session_id l_session_id \
    -target L_STN_ct \
    -image_traj l_final_traj \
    -final_depth l_final_depth \
    -ref_image intraop_ct \
    -anat_test STN \
    -com \
    -coord_dir com/

$script_dir/report_coords.py \
    -track left_track_t1.clp.gii \
    -trajs l_traj \
    -depths l_depth \
    -session_id l_session_id \
    -target L_STN_t1 \
    -image_traj l_final_traj \
    -final_depth l_final_depth \
    -ref_image postop_t1 \
    -anat_test STN \
    -com \
    -coord_dir com/

$script_dir/report_coords.py \
    -track right_track_ct.clp.gii \
    -trajs r_traj \
    -depths r_depth \
    -session_id r_session_id \
    -target R_STN_ct \
    -image_traj r_final_traj \
    -final_depth r_final_depth \
    -ref_image intraop_ct \
    -anat_test STN \
    -com \
    -coord_dir com/

$script_dir/report_coords.py \
    -track right_track_t1.clp.gii \
    -trajs r_traj \
    -depths r_depth \
    -session_id r_session_id \
    -target R_STN_t1 \
    -image_traj r_final_traj \
    -final_depth r_final_depth \
    -ref_image postop_t1 \
    -anat_test STN \
    -com \
    -coord_dir com/

# combine atlas spheres

$script_dir/combine_spheres_subject.py \
    -track1 left_track_ct.clp.gii l_final_traj \
    -track2 left_track_t1.clp.gii l_final_traj \
    -track3 right_track_ct.clp.gii r_final_traj \
    -track4 right_track_t1.clp.gii r_final_traj \
    -final_spheres \
    -record_spheres

# create some figures, consolidate all spreadsheet output

set sj_dir = `pwd`

cd ..

$sj_dir/$script_dir/combine_spheres_group.py \
    -final_fig $sj_dir/final_fig_stn \
    -anat_label stn

$sj_dir/$script_dir/combine_spheres_group.py \
    -record_fig $sj_dir/record_fig_stn \
    -anat_label stn

$sj_dir/$script_dir/combine_report_coords_dbstar.py

cd $sj_dir

DBStar
======
last update (v1.0.0a; 2017-02-08): initial release, updated readme

### e-mail peter_lauro@brown.edu for sample data

## Citation instructions:

If you find this software useful, please cite the accompanying papers in your manuscripts/presentations:

* DBStar (Lauro et al., 2018; https://doi.org/10.1159/000486645

*  @DBSproc (Lauro et al., 2015; https://doi.org/10.1002/hbm.23039) for electrode isolation methods

* Please cite AFNI (Cox, 1996; Computers and Biomedical Research)
		*		SUMA (Saad and Reynolds, 2012; https://doi.org/10.1016/j.neuroimage.2011.09.016))
		* FATCAT (Taylor and Saad, 2013; https://doi.org/10.1089/brain.2013.0154) as appropriate.

* Freesurfer  (https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation)

* MRIcron/dcm2nii (Rorden and Brett, 2000; Behavioral Neurology)

## Software to install:
#### Linux or macOS work best, Windows not tested

* AFNI/SUMA/FATCAT (latest version; https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html)

* Freesurfer (6.0.0; https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall)
* dcm2nii (https://www.nitrc.org/projects/dcm2nii/)
	* also consider the newer dcm2niix (not tested with DBStar - https://github.com/rordenlab/dcm2niix, http://neuro.debian.net/pkgs/dcm2niix.html)

* python 2.7 or 3
	* python3 --> also install the pyexcel-ods3 package (https://github.com/pyexcel/pyexcel-ods3)
	* python2.7 --> also install the pyexcel-ods package (https://github.com/pyexcel/pyexcel-ods)

* Libreoffice (https://www.libreoffice.org/)

* tcsh

## General Processing Steps:

0. (optional) Load DICOMs into Waypoint Planner, register the images to the pre-op CT and create your own PPR file (may require pv.cfg modification - set export_ppr = 1)

1. Reconstruct dicoms as niftis`(<./build_niftis.sh>` can be used with sample data)

2. Save patient-specific recording site data/final implantation data (`<demo_prep.sh>`)

3. Run processing steps described in  `<demo_proc.tcsh>`
	* Highly recommended that you run each processs step-by-step to understand each function and appropriately quality-check)

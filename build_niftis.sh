#!/bin/sh

# last update (v1.0.0; 2017-12-22): initial release

# IMPORTANT: requires a copy of dcm2nii in your executable binary directories (e.g. /usr/bin).

## reconstruct all niftis from dicom subdirectories
## (for use with sample data)

# extract images from image tarball
tar -xvzf demo_dicoms.tar.gz

# step through each dicom subdirectory, and rebuild a nifti using dcm2nii
cd dicoms/

for d in */; do

    cd $d

    # prevent gzipping (-g N) and reorienting/cropping (-x N) of resulting niftis
    dcm2nii -g N -x N *

    # further ensure cropped and reoriented niftis aren't saved
    rm co*nii
    rm o*nii

    mv ./*nii ../../.

    cd ..

done
